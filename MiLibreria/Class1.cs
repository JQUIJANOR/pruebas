﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;




namespace MiLibreria
{
    
    public class Utilidades
    {
        public static Boolean ValidarFormulario (Control Objeto, ErrorProvider ErrorProvider)
        {
            Boolean bHayErrores = false;
            //Recorre todos los controles del objeto
            foreach (Control cObj in Objeto.Controls)
            {
                if (cObj is ErrorTxtBox)
                {
                    //Instacia el objeto ErrorTxtBox
                    ErrorTxtBox eObjErrorTxt = (ErrorTxtBox)cObj;
                    if (eObjErrorTxt.Validar == true)
                    {
                        if (string.IsNullOrEmpty(cObj.Text.Trim()))
                        {
                            ErrorProvider.SetError(eObjErrorTxt, "No puede estar vacio");
                            bHayErrores = true;

                        }
                    }
                    else
                    {
                        ErrorProvider.SetError(eObjErrorTxt, "");
                        return false;
                    }

                }
                else if (cObj is GroupBox)
                {
                    foreach (Control txt in cObj.Controls)
                    {
                        if (txt is ErrorTxtBox )
                        {
                            //Instacia el objeto ErrorTxtBox
                            ErrorTxtBox eObjErrorTxt = (ErrorTxtBox)txt;
                            if (eObjErrorTxt.Validar == true)
                            {
                                if (string.IsNullOrEmpty(eObjErrorTxt.Text.Trim()))
                                {
                                    ErrorProvider.SetError(eObjErrorTxt, "No puede estar vacio");
                                    bHayErrores = true;

                                }
                            }
                            else
                            {
                                ErrorProvider.SetError(eObjErrorTxt, "");
                                return false;
                            }
                        }
                    }

                }

            }
            return bHayErrores;
            
        }


        public static DataSet Ejecutar(string cmd)
        {
            try
            {
                string cadena = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                //SqlConnection cnn = new SqlConnection(@"Server=DESKTOP-IICALOM\SQLEXPRESS;Database=TslColegiosDB;Uid=sa;Pwd=grasco;");
                SqlConnection cnn = new SqlConnection(cadena);
                cnn.Open();
                DataSet ds = new DataSet();
                SqlDataAdapter DP = new SqlDataAdapter(cmd, cnn);
                DP.Fill(ds);
                cnn.Close();
                return ds;

            }
            catch
            {
                return null;
            };

        }
        
        
        public static Boolean LimpiarTxtFormulario(Control Objeto)
        {
            Boolean bHayErrores = false;
            //Recorre todos los controles del objeto
            foreach (Control cObj in Objeto.Controls)
            {
                if (cObj is ErrorTxtBox || cObj is TextBox)
                {
                    //Instacia el objeto ErrorTxtBox
                    TextBox ObjetoTxt = (TextBox)cObj;
                    ObjetoTxt.Clear();
                }
                else if (cObj is GroupBox)
                {
                    foreach (Control txt in cObj.Controls)
                    {
                        if (txt is ErrorTxtBox || txt is TextBox)
                        {
                            //Instacia el objeto ErrorTxtBox
                            TextBox ObjetoTxt = (TextBox)txt;
                            ObjetoTxt.Clear();
                        }
                    }

                }

            }
            return bHayErrores;
        }
        public static bool Buscardgv(string TextoABuscar, string Columna, DataGridView grid)
        {
            bool encontrado = false;
            if (TextoABuscar == string.Empty) return false;
            if (grid.RowCount == 0) return false;
            grid.ClearSelection();
            if (Columna == string.Empty)
            {
                foreach (DataGridViewRow row in grid.Rows)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                        if (!(cell.Value == null))
                        {
                            if (cell.Value.ToString() == TextoABuscar)
                            {
                                row.Selected = true;
                                return true;
                            }
                        }
                }
            }
            else
            {
                foreach (DataGridViewRow row in grid.Rows)
                {

                    if (!(row.Cells[Columna].Value == null))
                    {
                        if (row.Cells[Columna].Value.ToString() == TextoABuscar)
                        {
                            row.Selected = true;
                            return true;
                        }
                    }
                }
            }
            return encontrado;
        }



    }


    /*****************************************/
    public class conecta
    {
	    public conecta() { }

	    public SqlConnection getConexion()
	    {
		    try
		    {
                string cadena = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(cadena);
			    return con;
		    }
		    catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		    catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		    return null;
	    }
    }
    public class Listas
    {
        public enum NivelUsuario
        {
            Usuario = 1,
            Operador = 2,
            Administrador = 3
        }

    }



}

