﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using MiLibreria;
using System.Configuration;

namespace TeslaColegios
{
    class Paises
    {
        conecta dbm = new conecta();
        public int Codigo_Pais { get; set; }
        public string Nombre_Pais { get; set; }

        public Paises(){ }


        public Boolean Crear(int vCODIGO_PAIS, string vNOMBRE_PAIS)
        {
            SqlConnection con = new SqlConnection();
            SqlParameter parametros;
            Boolean res = false;
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("paAdm_pais_Nuevo", con);
                parametros = cmd.Parameters.Add("@CODIGO_PAIS", SqlDbType.Int);
                parametros.Value = vCODIGO_PAIS;
                parametros = cmd.Parameters.Add("@NOMBRE_PAIS", SqlDbType.VarChar);
                parametros.Value = vNOMBRE_PAIS;
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd.ExecuteNonQuery() > 0)
                    res = true;
                con.Close();
            }
            catch (SqlException e) { throw e; }
            catch (Exception e) { throw e; }
            return res;
        }


        public Boolean Modificar(int vCODIGO_PAIS, string vNOMBRE_PAIS)
        {
            SqlConnection con = new SqlConnection();
            SqlParameter parametros;
            Boolean res = false;
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("paAdm_pais_Modificar", con);
                parametros = cmd.Parameters.Add("@CODIGO_PAIS", SqlDbType.Int);
                parametros.Value = vCODIGO_PAIS;
                parametros = cmd.Parameters.Add("@NOMBRE_PAIS", SqlDbType.VarChar);
                parametros.Value = vNOMBRE_PAIS;
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd.ExecuteNonQuery() > 0)
                    res = true;
                con.Close();
            }
            catch (SqlException e) { throw e; }
            catch (Exception e) { throw e; }
            return res;
        }


        public Boolean Eliminar(int vCODIGO_PAIS)
        {
            SqlConnection con = new SqlConnection();
            SqlParameter parametros;
            Boolean res = false;
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("paAdm_pais_Eliminar", con);
                parametros = cmd.Parameters.Add("@CODIGO_PAIS", SqlDbType.Int);
                parametros.Value = vCODIGO_PAIS;
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd.ExecuteNonQuery() > 0)
                    res = true;
                con.Close();
            }
            catch (SqlException e) { throw e; }
            catch (Exception e) { throw e; }
            return res;
        }


        public DataSet BuscarTodos()
        {
            SqlConnection con = new SqlConnection();
            DataSet ds = new DataSet();
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("paAdm_Pais", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                ad.Fill(ds);
                con.Close();
                return ds;
            }
            catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
            catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
            return null;
        }


        public Boolean BuscarCODIGO_PAIS(int vCODIGO_PAIS)
        {
            SqlConnection con = new SqlConnection();
            SqlParameter parametros;
            Boolean res = false;
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("spAdm_pais_TC", con);
                parametros = cmd.Parameters.Add("@CODIGO_PAIS", SqlDbType.Int);
                parametros.Value = vCODIGO_PAIS;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Codigo_Pais = dr.GetInt32(0);
                    Nombre_Pais = dr.GetString(1);
                    res = true;
                }
                con.Close();
            }
            catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
            catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
            return res;
        }

    }
}
