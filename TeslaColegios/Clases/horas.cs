﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using MiLibreria;
using System.Configuration;
using System.Data.OleDb;
using System.Xml.Linq;




namespace TeslaColegios
{
    class Hor_Horas
    {
        conecta dbm = new conecta();

        private int _DIA_SEMANA;
        private int _HORA;
        private DateTime _HORA_INICIAL;
        private DateTime _HORA_FINAL;

        public int DIA_SEMANA
        {
            get {return _DIA_SEMANA ;} 
            set { _DIA_SEMANA = value; }
        }
        public int HORA
        {
            get {return _HORA; }
            set { _HORA = value; }
        }
        public DateTime HORA_INICIAL
        {
            get { return _HORA_INICIAL; }
            set { _HORA_INICIAL = value; }
        }
        public DateTime HORA_FINAL
        {
            get { return _HORA_FINAL; }
            set { _HORA_FINAL = value; }
        }

        public Hor_Horas() { }

        public Boolean Crear(int vDIA_SEMAN, int vHORA, DateTime vHORA_INICIAL, DateTime vHORA_FINAL)
        {
            SqlConnection con = new SqlConnection();
            SqlParameter parametros;
            Boolean res = false;
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("pa_horas_Nuevo", con);
                parametros = cmd.Parameters.Add("@DIA_SEMAN", SqlDbType.Int);
                parametros.Value = vDIA_SEMAN;
                parametros = cmd.Parameters.Add("@HORA", SqlDbType.Int);
                parametros.Value = vHORA;
                parametros = cmd.Parameters.Add("@HORA_INICIAL", SqlDbType.DateTime);
                parametros.Value = vHORA_INICIAL;
                parametros = cmd.Parameters.Add("@HORA_FINAL", SqlDbType.DateTime);
                parametros.Value = vHORA_FINAL;
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd.ExecuteNonQuery() > 0)
                    res = true;
                con.Close();
            }
            catch (SqlException e) { throw e;  }
            catch (Exception e) { throw e;  }
            return res;
        }


        public Boolean Modificar(int vDIA_SEMAN, int vHORA, DateTime vHORA_INICIAL, DateTime vHORA_FINAL)
        {
            SqlConnection con = new SqlConnection();
            SqlParameter parametros;
            Boolean res = false;
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("pa_horas_Modifica", con);
                parametros = cmd.Parameters.Add("@DIA_SEMAN", SqlDbType.Int);
                parametros.Value = vDIA_SEMAN;
                parametros = cmd.Parameters.Add("@HORA", SqlDbType.Int);
                parametros.Value = vHORA;
                parametros = cmd.Parameters.Add("@HORA_INICIAL", SqlDbType.DateTime);
                parametros.Value = vHORA_INICIAL;
                parametros = cmd.Parameters.Add("@HORA_FINAL", SqlDbType.DateTime);
                parametros.Value = vHORA_FINAL;
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd.ExecuteNonQuery() > 0)
                    res = true;
                con.Close();
            }
            catch (SqlException e) { throw e; }
            catch (Exception e) { throw e; }
            return res;
        }


        public Boolean Eliminar(int vDIA_SEMAN, int vHORA)
        {
            SqlConnection con = new SqlConnection();
            SqlParameter parametros;
            Boolean res = false;
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("pa_horas_Elimina", con);
                parametros = cmd.Parameters.Add("@DIA_SEMAN", SqlDbType.Int);
                parametros.Value = vDIA_SEMAN;
                parametros = cmd.Parameters.Add("@HORA", SqlDbType.Int);
                parametros.Value = vHORA;
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd.ExecuteNonQuery() > 0)
                    res = true;
                con.Close();
            }
            catch (SqlException e) { throw e; }
            catch (Exception e) { throw e; }
            return res;
        }


        public DataSet BuscarTodos()
        {
            SqlConnection con = new SqlConnection();
            DataSet ds = new DataSet();
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("pa_Horas_TT", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                ad.Fill(ds);
                con.Close();
                return ds;
            }
            catch (SqlException e) { throw e; }
            catch (Exception e) { throw e; }
        }


        public Boolean BuscarDIA_SEMAN(int vDIA_SEMAN)
        {
            SqlConnection con = new SqlConnection();
            SqlParameter parametros;
            Boolean res = false;
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("pa_Horas_TC", con);
                parametros = cmd.Parameters.Add("@DIA_SEMAN", SqlDbType.Int);
                parametros.Value = vDIA_SEMAN;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    _DIA_SEMANA = dr.GetInt32(0);
                    _HORA = dr.GetInt32(1);
                    _HORA_INICIAL = dr.GetDateTime(2);
                    _HORA_FINAL = dr.GetDateTime(3);
                    res = true;
                }
                con.Close();
            }
            catch (SqlException e) { throw e; }
            catch (Exception e) { throw e; }
            return res;
        }

        public List<Hor_Horas> ObtieneListaHoras()
        {
            DataSet ds = new DataSet();
            ds = this.BuscarTodos();
            List<Hor_Horas> Lista = new List<Hor_Horas>();

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                Lista.Add ( new Hor_Horas
                {
                DIA_SEMANA = Convert.ToInt32(item["DIA_SEMANA"].ToString()),
                HORA = Convert.ToInt32(item["HORA"].ToString()),
                HORA_INICIAL = Convert.ToDateTime(item["HORA_INICIAL"].ToString()),
                HORA_FINAL = Convert.ToDateTime(item["HORA_FINAL"].ToString()),
                });
            }

            return Lista.ToList(); 
        }



    } 

}
