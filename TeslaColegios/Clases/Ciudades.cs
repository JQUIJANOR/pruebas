﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using MiLibreria;
using System.Configuration;

namespace TeslaColegios
{
    class Ciudades
    {
        conecta dbm = new conecta();
        public int CODIGO_MUNICIPIO  { get; set; }
        public int CODIGO_DEPARTAMENTO  { get; set; }
        public string NOMBRE_MUNICIPIO  { get; set; }

        public Ciudades() { }

        public Boolean Crear(int vCODIGO_MUNICIPIO, int vCODIGO_DEPARTAMENTO, string vNOMBRE_MUNICIPIO)
        {
            SqlConnection con = new SqlConnection();
            SqlParameter parametros;
            Boolean res = false;
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("spAdm_municipios_Nuevo", con);
                parametros = cmd.Parameters.Add("@CODIGO_MUNICIPIO", SqlDbType.Int);
                parametros.Value = vCODIGO_MUNICIPIO;
                parametros = cmd.Parameters.Add("@CODIGO_DEPARTAMENTO", SqlDbType.Int);
                parametros.Value = vCODIGO_DEPARTAMENTO;
                parametros = cmd.Parameters.Add("@NOMBRE_MUNICIPIO", SqlDbType.VarChar);
                parametros.Value = vNOMBRE_MUNICIPIO;
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd.ExecuteNonQuery() > 0)
                    res = true;
                con.Close();
            }
            catch (SqlException e) { throw e; }
            catch (Exception e) { throw e; }
            return res;
        }


        public Boolean Modificar(int vCODIGO_MUNICIPIO, int vCODIGO_DEPARTAMENTO, string vNOMBRE_MUNICIPIO)
        {
            SqlConnection con = new SqlConnection();
            SqlParameter parametros;
            Boolean res = false;
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("spAdm_municipios_M", con);
                parametros = cmd.Parameters.Add("@CODIGO_MUNICIPIO", SqlDbType.Int);
                parametros.Value = vCODIGO_MUNICIPIO;
                parametros = cmd.Parameters.Add("@CODIGO_DEPARTAMENTO", SqlDbType.Int);
                parametros.Value = vCODIGO_DEPARTAMENTO;
                parametros = cmd.Parameters.Add("@NOMBRE_MUNICIPIO", SqlDbType.VarChar);
                parametros.Value = vNOMBRE_MUNICIPIO;
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd.ExecuteNonQuery() > 0)
                    res = true;
                con.Close();
            }
            catch (SqlException e) { throw e; }
            catch (Exception e) { throw e; }
            return res;
        }


        public Boolean Eliminar(int vCODIGO_DEPARTAMENTO, int vCODIGO_MUNICIPIO)
        {
            SqlConnection con = new SqlConnection();
            SqlParameter parametros;
            Boolean res = false;
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("paAdm_municipios_Elimina", con);
                parametros = cmd.Parameters.Add("@CODIGO_DEPARTAMENTO", SqlDbType.Int);
                parametros.Value = vCODIGO_DEPARTAMENTO;
                parametros = cmd.Parameters.Add("@CODIGO_MUNICIPIO", SqlDbType.Int);
                parametros.Value = vCODIGO_MUNICIPIO;
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd.ExecuteNonQuery() > 0)
                    res = true;
                con.Close();
            }
            catch (SqlException e) { throw e; }
            catch (Exception e) { throw e; }
            return res;
        }


        public DataSet BuscarTodos()
        {
            SqlConnection con = new SqlConnection();
            DataSet ds = new DataSet();
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("paAdm_Municipios_TT", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                ad.Fill(ds);
                con.Close();
                return ds;
            }
            catch (SqlException e) { throw e; }
            catch (Exception e) { throw e; }
        }


        public Boolean BuscarCODIGO_MUNICIPIO(int vCODIGO_MUNICIPIO)
        {
            SqlConnection con = new SqlConnection();
            SqlParameter parametros;
            Boolean res = false;
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("paAdm_Municipios_TC", con);
                parametros = cmd.Parameters.Add("@CODIGO_MUNICIPIO", SqlDbType.Int);
                parametros.Value = vCODIGO_MUNICIPIO;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    CODIGO_MUNICIPIO = dr.GetInt32(0);
                    CODIGO_DEPARTAMENTO = dr.GetInt32(1);
                    NOMBRE_MUNICIPIO = dr.GetString(2);
                    res = true;
                }
                con.Close();
            }
            catch (SqlException e) { throw e; }
            catch (Exception e) { throw e; }
            return res;
        }

        public DataSet BuscarCiudadesDpto(int iCodigoDpto)
        {
            SqlConnection con = new SqlConnection();
            SqlParameter parametros;

            DataSet ds = new DataSet();
            try
            {
                con = dbm.getConexion();
                con.Open();
                SqlCommand cmd = new SqlCommand("paAdm_ciudades_Dpto", con);
                parametros = cmd.Parameters.Add("@CODIGO_DPTO", SqlDbType.Int);
                parametros.Value = iCodigoDpto;
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                ad.Fill(ds);
                con.Close();
                return ds;
            }
            catch (SqlException e) { throw e; }
            catch (Exception e) { throw e; }


        }


    }
}
