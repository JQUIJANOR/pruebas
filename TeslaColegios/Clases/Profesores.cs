﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using MiLibreria;
using System.Configuration;

namespace TeslaColegios
{
    class Profesores
    {
        conecta dbm = new conecta();


        private decimal _ID_PROFESOR;
	    private  int _TIPO_IDENTIFICACION;
	    private string _NUMERO_IDENTIFICACION;
	    private int _CODIGO_PAIS;
	    private int _CODIGO_DEPARTAMENTO;
	    private int _CODIGO_MUNICIPIO;
	    private string _PRIMER_NOMBRE;
	    private string _SEGUNDO_NOMBRE;
	    private string _PRIMER_APELLIDO;
	    private string _SEGUNDO_APELLIDO;


	    public decimal ID_EMPLEADO
	    {
		    get { return _ID_PROFESOR; }
		    set { _ID_PROFESOR = value; }
	    }
	    public  int TIPO_IDENTIFICACION
	    {
		    get { return _TIPO_IDENTIFICACION; }
		    set { _TIPO_IDENTIFICACION = value; }
	    }
        public string NUMERO_IDENTIFICACION
        {
            get { return _NUMERO_IDENTIFICACION; }
            set { _NUMERO_IDENTIFICACION = value; }
        }

	    public int CODIGO_PAIS
	    {
		    get { return _CODIGO_PAIS; }
		    set { _CODIGO_PAIS = value; }
	    }
	    public int CODIGO_DEPARTAMENTO
	    {
		    get { return _CODIGO_DEPARTAMENTO; }
		    set { _CODIGO_DEPARTAMENTO = value; }
	    }
	    public int CODIGO_MUNICIPIO
	    {
		    get { return _CODIGO_MUNICIPIO; }
		    set { _CODIGO_MUNICIPIO = value; }
	    }
	    public string PRIMER_NOMBRE
	    {
		    get { return _PRIMER_NOMBRE; }
		    set { _PRIMER_NOMBRE = value; }
	    }
	    public string SEGUNDO_NOMBRE
	    {
		    get { return _SEGUNDO_NOMBRE; }
		    set { _SEGUNDO_NOMBRE = value; }
	    }
	    public string PRIMER_APELLIDO
	    {
		    get { return _PRIMER_APELLIDO; }
		    set { _PRIMER_APELLIDO = value; }
	    }
	    public string SEGUNDO_APELLIDO
	    {
		    get { return _SEGUNDO_APELLIDO; }
		    set { _SEGUNDO_APELLIDO = value; }
	    }

	    public Profesores(){ }

	public Boolean Crear( decimal vID_EMPLEADO , int vTIPO_IDENTIFICACION ,string vNUMERO_IDENTIFICACION ,int vCODIGO_PAIS ,int vCODIGO_DEPARTAMENTO ,int vCODIGO_MUNICIPIO ,string vPRIMER_NOMBRE ,string vSEGUNDO_NOMBRE ,string vPRIMER_APELLIDO ,string vSEGUNDO_APELLIDO)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("paPro_profesores_Nuevo", con);
            parametros = cmd.Parameters.Add("@ID_PROFESOR", SqlDbType.Decimal);
			parametros.Value = vID_EMPLEADO;
			parametros = cmd.Parameters.Add("@TIPO_IDENTIFICACION", SqlDbType.Int);
			parametros.Value = vTIPO_IDENTIFICACION;
			parametros = cmd.Parameters.Add("@NUMERO_IDENTIFICACION", SqlDbType.VarChar);
			parametros.Value = vNUMERO_IDENTIFICACION;
			parametros = cmd.Parameters.Add("@CODIGO_PAIS", SqlDbType.Int);
			parametros.Value = vCODIGO_PAIS;
			parametros = cmd.Parameters.Add("@CODIGO_DEPARTAMENTO", SqlDbType.Int);
			parametros.Value = vCODIGO_DEPARTAMENTO;
			parametros = cmd.Parameters.Add("@CODIGO_MUNICIPIO", SqlDbType.Int);
			parametros.Value = vCODIGO_MUNICIPIO;
			parametros = cmd.Parameters.Add("@PRIMER_NOMBRE", SqlDbType.VarChar);
			parametros.Value = vPRIMER_NOMBRE;
			parametros = cmd.Parameters.Add("@SEGUNDO_NOMBRE", SqlDbType.VarChar);
			parametros.Value = vSEGUNDO_NOMBRE;
			parametros = cmd.Parameters.Add("@PRIMER_APELLIDO", SqlDbType.VarChar);
			parametros.Value = vPRIMER_APELLIDO;
			parametros = cmd.Parameters.Add("@SEGUNDO_APELLIDO", SqlDbType.VarChar);
			parametros.Value = vSEGUNDO_APELLIDO;
			cmd.CommandType = CommandType.StoredProcedure;
			if (cmd.ExecuteNonQuery() > 0)
				res = true;
			con.Close();
		}
		catch (SqlException e) { throw e; }
		catch (Exception e) { throw e; }
		return res;
	}


	public Boolean Modificar( decimal vID_EMPLEADO , int vTIPO_IDENTIFICACION ,string vNUMERO_IDENTIFICACION ,int vCODIGO_PAIS ,int vCODIGO_DEPARTAMENTO ,int vCODIGO_MUNICIPIO ,string vPRIMER_NOMBRE ,string vSEGUNDO_NOMBRE ,string vPRIMER_APELLIDO ,string vSEGUNDO_APELLIDO)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
            SqlCommand cmd = new SqlCommand("paPro_profesores_Modifica", con);
			parametros = cmd.Parameters.Add("@ID_EMPLEADO", SqlDbType.Decimal);
			parametros.Value = vID_EMPLEADO;
			parametros = cmd.Parameters.Add("@TIPO_IDENTIFICACION", SqlDbType.Int);
			parametros.Value = vTIPO_IDENTIFICACION;
			parametros = cmd.Parameters.Add("@NUMERO_IDENTIFICACION", SqlDbType.VarChar);
			parametros.Value = vNUMERO_IDENTIFICACION;
			parametros = cmd.Parameters.Add("@CODIGO_PAIS", SqlDbType.Int);
			parametros.Value = vCODIGO_PAIS;
			parametros = cmd.Parameters.Add("@CODIGO_DEPARTAMENTO", SqlDbType.Int);
			parametros.Value = vCODIGO_DEPARTAMENTO;
			parametros = cmd.Parameters.Add("@CODIGO_MUNICIPIO", SqlDbType.Int);
			parametros.Value = vCODIGO_MUNICIPIO;
			parametros = cmd.Parameters.Add("@PRIMER_NOMBRE", SqlDbType.VarChar);
			parametros.Value = vPRIMER_NOMBRE;
			parametros = cmd.Parameters.Add("@SEGUNDO_NOMBRE", SqlDbType.VarChar);
			parametros.Value = vSEGUNDO_NOMBRE;
			parametros = cmd.Parameters.Add("@PRIMER_APELLIDO", SqlDbType.VarChar);
			parametros.Value = vPRIMER_APELLIDO;
			parametros = cmd.Parameters.Add("@SEGUNDO_APELLIDO", SqlDbType.VarChar);
			parametros.Value = vSEGUNDO_APELLIDO;
			cmd.CommandType = CommandType.StoredProcedure;
			if (cmd.ExecuteNonQuery() > 0)
				res = true;
			con.Close();
		}
		catch (SqlException e) { throw e; }
		catch (Exception e) { throw e; }
		return res;
	}


	public Boolean Eliminar( decimal vID_EMPLEADO)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
            SqlCommand cmd = new SqlCommand("paPro_profesores_Elimina", con);
			parametros = cmd.Parameters.Add("@ID_EMPLEADO", SqlDbType.Decimal);
			parametros.Value = vID_EMPLEADO;
			cmd.CommandType = CommandType.StoredProcedure;
			if (cmd.ExecuteNonQuery() > 0)
				res = true;
			con.Close();
		}
		catch (SqlException e) { throw e; }
		catch (Exception e) { throw e;}
		return res;
	}


	public DataSet BuscarTodos()
	{
		SqlConnection con = new SqlConnection();
		DataSet ds = new DataSet();
		try
		{
			con = dbm.getConexion();
			con.Open();
            SqlCommand cmd = new SqlCommand("paPro_Profesores_TT", con);
			cmd.CommandType = CommandType.StoredProcedure;
			SqlDataAdapter ad = new SqlDataAdapter(cmd);
			ad.Fill(ds);
			con.Close();
			return ds;
		}
		catch (SqlException e) { throw e; }
		catch (Exception e) { throw e;}
	}


	public Boolean BuscarID_PROFESOR( decimal vID_EMPLEADO)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
            SqlCommand cmd = new SqlCommand("paPro_Profesores_TC", con);
			parametros = cmd.Parameters.Add("@ID_EMPLEADO", SqlDbType.Decimal);
			parametros.Value = vID_EMPLEADO;
			cmd.CommandType = CommandType.StoredProcedure;
			SqlDataReader dr = cmd.ExecuteReader();
			while (dr.Read())
			{
				ID_EMPLEADO = dr.GetDecimal(0);
				TIPO_IDENTIFICACION = dr.GetInt32(1);
				NUMERO_IDENTIFICACION = dr.GetString(2);
				CODIGO_PAIS = dr.GetInt32(3);
				CODIGO_DEPARTAMENTO = dr.GetInt32(4);
				CODIGO_MUNICIPIO = dr.GetInt32(5);
				PRIMER_NOMBRE = dr.GetString(6);
				SEGUNDO_NOMBRE = dr.GetString(7);
				PRIMER_APELLIDO = dr.GetString(8);
				SEGUNDO_APELLIDO = dr.GetString(9);
				res = true;
			}
			con.Close();
		}
        catch (SqlException e) { throw e; }
        catch (Exception e) { throw e; }
		return res;
	}


    }
}
