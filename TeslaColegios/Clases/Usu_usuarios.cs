﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using MiLibreria;
using System.Configuration;


namespace TeslaColegios.Clases
{
    public class Usu_usuarios
{
	conecta dbm = new conecta();

/*
	private string Id_Usuario;
	private int Tipo_Identificacion;
	private decimal Identificacion;
	private string Nombre;
	private long Password;
	private int Nivel_Usuario;
*/
    public string Id_Usuario { get; set; }
	public int Tipo_Identificacion { get; set; }
	public decimal Identificacion { get; set; }
	public string Nombre { get; set; }
    public int Nivel_Usuario { get; set; }
    public string Password { get; set; }
	
    public Usu_usuarios(){ }

	public Boolean Crear(string vId_Usuario , int vTipo_Identificacion , decimal vIdentificacion ,string vNombre , string vPassword ,int vNivel_Usuario)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spUsu_usuarios_N", con);
			parametros = cmd.Parameters.Add("@Id_Usuario", SqlDbType.VarChar);
			parametros.Value = vId_Usuario;
			parametros = cmd.Parameters.Add("@Tipo_Identificacion", SqlDbType.Int);
			parametros.Value = vTipo_Identificacion;
			parametros = cmd.Parameters.Add("@Identificacion", SqlDbType.Decimal);
			parametros.Value = vIdentificacion;
			parametros = cmd.Parameters.Add("@Nombre", SqlDbType.VarChar);
			parametros.Value = vNombre;
			parametros = cmd.Parameters.Add("@Password", SqlDbType.VarBinary);
			parametros.Value = vPassword;
			parametros = cmd.Parameters.Add("@Nivel_Usuario", SqlDbType.Int);
			parametros.Value = vNivel_Usuario;
			cmd.CommandType = CommandType.StoredProcedure;
			if (cmd.ExecuteNonQuery() > 0)
				res = true;
			con.Close();
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return res;
	}


	public Boolean Modificar(string vId_Usuario , int vTipo_Identificacion , decimal vIdentificacion ,string vNombre , string vPassword ,int vNivel_Usuario)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spUsu_usuarios_M", con);
			parametros = cmd.Parameters.Add("@Id_Usuario", SqlDbType.VarChar);
			parametros.Value = vId_Usuario;
			parametros = cmd.Parameters.Add("@Tipo_Identificacion", SqlDbType.Int);
			parametros.Value = vTipo_Identificacion;
			parametros = cmd.Parameters.Add("@Identificacion", SqlDbType.Decimal);
			parametros.Value = vIdentificacion;
			parametros = cmd.Parameters.Add("@Nombre", SqlDbType.VarChar);
			parametros.Value = vNombre;
			parametros = cmd.Parameters.Add("@Password", SqlDbType.VarBinary);
			parametros.Value = vPassword;
			parametros = cmd.Parameters.Add("@Nivel_Usuario", SqlDbType.Int);
			parametros.Value = vNivel_Usuario;
			cmd.CommandType = CommandType.StoredProcedure;
			if (cmd.ExecuteNonQuery() > 0)
				res = true;
			con.Close();
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return res;
	}


	public Boolean Eliminar(string vId_Usuario)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spUsu_usuarios_E", con);
			parametros = cmd.Parameters.Add("@Id_Usuario", SqlDbType.VarChar);
			parametros.Value = vId_Usuario;
			cmd.CommandType = CommandType.StoredProcedure;
			if (cmd.ExecuteNonQuery() > 0)
				res = true;
			con.Close();
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return res;
	}


	public DataSet BuscarTodos()
	{
		SqlConnection con = new SqlConnection();
		DataSet ds = new DataSet();
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spUsu_usuarios_TT", con);
			cmd.CommandType = CommandType.StoredProcedure;
			SqlDataAdapter ad = new SqlDataAdapter(cmd);
			ad.Fill(ds);
			con.Close();
			return ds;
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return null;
	}


	public Boolean CargaClase_Usuario(string vId_Usuario)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
        byte[] buffer = new byte[4096];
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spUsu_usuarios_TC", con);
			parametros = cmd.Parameters.Add("@Id_Usuario", SqlDbType.VarChar);
			parametros.Value = vId_Usuario;
			cmd.CommandType = CommandType.StoredProcedure;
			SqlDataReader dr = cmd.ExecuteReader();
			while (dr.Read())
			{
				Id_Usuario = dr.GetString(0);
				Tipo_Identificacion = dr.GetInt16(1);
				Identificacion = dr.GetDecimal(2);
				Nombre = dr.GetString(3);
                Password = dr.GetString(4);
				Nivel_Usuario = dr.GetInt32(5);
				res = true;
			}
			con.Close();
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return res;
	}
    public DataSet Validar_Usuario(string vId_Usuario, string vPassword)
    {
        SqlConnection con = new SqlConnection();
        DataSet ds = new DataSet();
        try
        {
            con.Close();
            con = dbm.getConexion();
            con.Open();
            string cdm = string.Format("exec dbo.uspLogin '{0}', '{1}'", vId_Usuario, vPassword);
            SqlCommand cmd = new SqlCommand(cdm, con);
            SqlDataAdapter ad = new SqlDataAdapter(cdm, con);
            ad.Fill(ds);
            con.Close();
            return ds;
        }
        catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
        catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
        return null;
    }
    public DataSet BuscarTodosTiposId()
    {
        SqlConnection con = new SqlConnection();
        DataSet ds = new DataSet();
        try
        {
            con = dbm.getConexion();
            con.Open();
            SqlCommand cmd = new SqlCommand("paTipos_Identificacion_All", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(ds);
            con.Close();
            return ds;
        }
        catch (SqlException e) { throw e; }
        catch (Exception e) { throw e; }
    }



}

}
