﻿namespace TeslaColegios.Academia
{
    partial class FrmProfesores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RbtNombre = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.RbtCodigo = new System.Windows.Forms.RadioButton();
            this.TxtBuscado = new System.Windows.Forms.TextBox();
            this.TxtIdProfesor = new MiLibreria.ErrorTxtBox();
            this.TxtNoIdentificacion = new MiLibreria.ErrorTxtBox();
            this.TxtCodigoPais = new System.Windows.Forms.TextBox();
            this.ComboPais = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CmbDepartamentos = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtCodDepto = new MiLibreria.ErrorTxtBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtTipoId = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.CmbTipoId = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtCodigoCiudad = new System.Windows.Forms.TextBox();
            this.CmbCiudades = new System.Windows.Forms.ComboBox();
            this.TxtPrimerNombre = new MiLibreria.ErrorTxtBox();
            this.TxtPrimerApeliido = new MiLibreria.ErrorTxtBox();
            this.txtSegundoNombre = new System.Windows.Forms.TextBox();
            this.TxtSegundoApellido = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGuardar
            // 
            this.btnGuardar.TabIndex = 2;
            // 
            // btnEliminar
            // 
            this.btnEliminar.TabIndex = 3;
            // 
            // BtnSalir
            // 
            this.BtnSalir.TabIndex = 8;
            // 
            // BtnNuevo
            // 
            this.BtnNuevo.TabIndex = 1;
            // 
            // BtnLast
            // 
            this.BtnLast.TabIndex = 7;
            // 
            // BtnNext
            // 
            this.BtnNext.TabIndex = 6;
            // 
            // BtnPrior
            // 
            this.BtnPrior.TabIndex = 5;
            // 
            // BtnFirst
            // 
            this.BtnFirst.TabIndex = 4;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 50);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(804, 146);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RbtNombre);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.RbtCodigo);
            this.groupBox2.Controls.Add(this.TxtBuscado);
            this.groupBox2.Location = new System.Drawing.Point(13, 202);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(563, 38);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            // 
            // RbtNombre
            // 
            this.RbtNombre.AutoSize = true;
            this.RbtNombre.Location = new System.Drawing.Point(148, 15);
            this.RbtNombre.Name = "RbtNombre";
            this.RbtNombre.Size = new System.Drawing.Size(62, 17);
            this.RbtNombre.TabIndex = 1;
            this.RbtNombre.TabStop = true;
            this.RbtNombre.Text = "Nombre";
            this.RbtNombre.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Buscar:";
            // 
            // RbtCodigo
            // 
            this.RbtCodigo.AutoSize = true;
            this.RbtCodigo.Location = new System.Drawing.Point(58, 14);
            this.RbtCodigo.Name = "RbtCodigo";
            this.RbtCodigo.Size = new System.Drawing.Size(88, 17);
            this.RbtCodigo.TabIndex = 0;
            this.RbtCodigo.TabStop = true;
            this.RbtCodigo.Text = "Identificacion";
            this.RbtCodigo.UseVisualStyleBackColor = true;
            // 
            // TxtBuscado
            // 
            this.TxtBuscado.Location = new System.Drawing.Point(354, 12);
            this.TxtBuscado.Name = "TxtBuscado";
            this.TxtBuscado.Size = new System.Drawing.Size(182, 20);
            this.TxtBuscado.TabIndex = 0;
            // 
            // TxtIdProfesor
            // 
            this.TxtIdProfesor.Location = new System.Drawing.Point(106, 257);
            this.TxtIdProfesor.Name = "TxtIdProfesor";
            this.TxtIdProfesor.Size = new System.Drawing.Size(100, 20);
            this.TxtIdProfesor.TabIndex = 1;
            this.TxtIdProfesor.Validar = true;
            // 
            // TxtNoIdentificacion
            // 
            this.TxtNoIdentificacion.Location = new System.Drawing.Point(610, 257);
            this.TxtNoIdentificacion.Name = "TxtNoIdentificacion";
            this.TxtNoIdentificacion.Size = new System.Drawing.Size(113, 20);
            this.TxtNoIdentificacion.TabIndex = 4;
            this.TxtNoIdentificacion.Validar = true;
            // 
            // TxtCodigoPais
            // 
            this.TxtCodigoPais.Enabled = false;
            this.TxtCodigoPais.Location = new System.Drawing.Point(107, 285);
            this.TxtCodigoPais.Name = "TxtCodigoPais";
            this.TxtCodigoPais.Size = new System.Drawing.Size(62, 20);
            this.TxtCodigoPais.TabIndex = 5;
            // 
            // ComboPais
            // 
            this.ComboPais.FormattingEnabled = true;
            this.ComboPais.Location = new System.Drawing.Point(177, 285);
            this.ComboPais.Name = "ComboPais";
            this.ComboPais.Size = new System.Drawing.Size(170, 21);
            this.ComboPais.TabIndex = 6;
            this.ComboPais.SelectionChangeCommitted += new System.EventHandler(this.ComboPais_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 285);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Pais";
            // 
            // CmbDepartamentos
            // 
            this.CmbDepartamentos.FormattingEnabled = true;
            this.CmbDepartamentos.Location = new System.Drawing.Point(526, 285);
            this.CmbDepartamentos.Name = "CmbDepartamentos";
            this.CmbDepartamentos.Size = new System.Drawing.Size(208, 21);
            this.CmbDepartamentos.TabIndex = 8;
            this.CmbDepartamentos.SelectionChangeCommitted += new System.EventHandler(this.CmbDepartamentos_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(363, 285);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Departamento";
            // 
            // TxtCodDepto
            // 
            this.TxtCodDepto.Enabled = false;
            this.TxtCodDepto.Location = new System.Drawing.Point(452, 285);
            this.TxtCodDepto.Name = "TxtCodDepto";
            this.TxtCodDepto.Size = new System.Drawing.Size(68, 20);
            this.TxtCodDepto.TabIndex = 7;
            this.TxtCodDepto.Validar = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 257);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Id Profesor";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(502, 257);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "No Identificacion";
            // 
            // TxtTipoId
            // 
            this.TxtTipoId.Enabled = false;
            this.TxtTipoId.Location = new System.Drawing.Point(273, 257);
            this.TxtTipoId.Name = "TxtTipoId";
            this.TxtTipoId.Size = new System.Drawing.Size(62, 20);
            this.TxtTipoId.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(224, 257);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Tipo Id.";
            // 
            // CmbTipoId
            // 
            this.CmbTipoId.FormattingEnabled = true;
            this.CmbTipoId.Location = new System.Drawing.Point(342, 257);
            this.CmbTipoId.Name = "CmbTipoId";
            this.CmbTipoId.Size = new System.Drawing.Size(121, 21);
            this.CmbTipoId.TabIndex = 3;
            this.CmbTipoId.SelectionChangeCommitted += new System.EventHandler(this.CmbTipoId_SelectionChangeCommitted);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 314);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Ciudad";
            // 
            // TxtCodigoCiudad
            // 
            this.TxtCodigoCiudad.Enabled = false;
            this.TxtCodigoCiudad.Location = new System.Drawing.Point(107, 311);
            this.TxtCodigoCiudad.Name = "TxtCodigoCiudad";
            this.TxtCodigoCiudad.Size = new System.Drawing.Size(62, 20);
            this.TxtCodigoCiudad.TabIndex = 9;
            // 
            // CmbCiudades
            // 
            this.CmbCiudades.FormattingEnabled = true;
            this.CmbCiudades.Location = new System.Drawing.Point(177, 312);
            this.CmbCiudades.Name = "CmbCiudades";
            this.CmbCiudades.Size = new System.Drawing.Size(170, 21);
            this.CmbCiudades.TabIndex = 10;
            // 
            // TxtPrimerNombre
            // 
            this.TxtPrimerNombre.Location = new System.Drawing.Point(107, 354);
            this.TxtPrimerNombre.Name = "TxtPrimerNombre";
            this.TxtPrimerNombre.Size = new System.Drawing.Size(100, 20);
            this.TxtPrimerNombre.TabIndex = 11;
            this.TxtPrimerNombre.Validar = false;
            // 
            // TxtPrimerApeliido
            // 
            this.TxtPrimerApeliido.Location = new System.Drawing.Point(379, 354);
            this.TxtPrimerApeliido.Name = "TxtPrimerApeliido";
            this.TxtPrimerApeliido.Size = new System.Drawing.Size(100, 20);
            this.TxtPrimerApeliido.TabIndex = 13;
            this.TxtPrimerApeliido.Validar = false;
            // 
            // txtSegundoNombre
            // 
            this.txtSegundoNombre.Location = new System.Drawing.Point(212, 354);
            this.txtSegundoNombre.Name = "txtSegundoNombre";
            this.txtSegundoNombre.Size = new System.Drawing.Size(100, 20);
            this.txtSegundoNombre.TabIndex = 12;
            // 
            // TxtSegundoApellido
            // 
            this.TxtSegundoApellido.Location = new System.Drawing.Point(486, 354);
            this.TxtSegundoApellido.Name = "TxtSegundoApellido";
            this.TxtSegundoApellido.Size = new System.Drawing.Size(100, 20);
            this.TxtSegundoApellido.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(109, 338);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "Primer Nombre";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(213, 338);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 34;
            this.label9.Text = "Segundo Nombre";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(379, 338);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "Primer Apellido";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(486, 338);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 13);
            this.label11.TabIndex = 36;
            this.label11.Text = "Segundo Apellido";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(18, 354);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 13);
            this.label12.TabIndex = 37;
            this.label12.Text = "Nombres";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(324, 354);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 38;
            this.label13.Text = "Apellidos";
            // 
            // FrmProfesores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 487);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TxtSegundoApellido);
            this.Controls.Add(this.txtSegundoNombre);
            this.Controls.Add(this.TxtPrimerApeliido);
            this.Controls.Add(this.TxtPrimerNombre);
            this.Controls.Add(this.CmbCiudades);
            this.Controls.Add(this.TxtCodigoCiudad);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CmbTipoId);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtTipoId);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CmbDepartamentos);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtCodDepto);
            this.Controls.Add(this.TxtCodigoPais);
            this.Controls.Add(this.ComboPais);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtNoIdentificacion);
            this.Controls.Add(this.TxtIdProfesor);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dataGridView1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmProfesores";
            this.Text = "FrmProfesores";
            this.Load += new System.EventHandler(this.FrmProfesores_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.dataGridView1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.TxtIdProfesor, 0);
            this.Controls.SetChildIndex(this.TxtNoIdentificacion, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.ComboPais, 0);
            this.Controls.SetChildIndex(this.TxtCodigoPais, 0);
            this.Controls.SetChildIndex(this.TxtCodDepto, 0);
            this.Controls.SetChildIndex(this.label2, 0);
            this.Controls.SetChildIndex(this.CmbDepartamentos, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.label5, 0);
            this.Controls.SetChildIndex(this.TxtTipoId, 0);
            this.Controls.SetChildIndex(this.label6, 0);
            this.Controls.SetChildIndex(this.CmbTipoId, 0);
            this.Controls.SetChildIndex(this.label7, 0);
            this.Controls.SetChildIndex(this.TxtCodigoCiudad, 0);
            this.Controls.SetChildIndex(this.CmbCiudades, 0);
            this.Controls.SetChildIndex(this.TxtPrimerNombre, 0);
            this.Controls.SetChildIndex(this.TxtPrimerApeliido, 0);
            this.Controls.SetChildIndex(this.txtSegundoNombre, 0);
            this.Controls.SetChildIndex(this.TxtSegundoApellido, 0);
            this.Controls.SetChildIndex(this.label8, 0);
            this.Controls.SetChildIndex(this.label9, 0);
            this.Controls.SetChildIndex(this.label10, 0);
            this.Controls.SetChildIndex(this.label11, 0);
            this.Controls.SetChildIndex(this.label12, 0);
            this.Controls.SetChildIndex(this.label13, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton RbtNombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton RbtCodigo;
        private System.Windows.Forms.TextBox TxtBuscado;
        private MiLibreria.ErrorTxtBox TxtIdProfesor;
        private MiLibreria.ErrorTxtBox TxtNoIdentificacion;
        private System.Windows.Forms.TextBox TxtCodigoPais;
        private System.Windows.Forms.ComboBox ComboPais;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CmbDepartamentos;
        private System.Windows.Forms.Label label2;
        private MiLibreria.ErrorTxtBox TxtCodDepto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtTipoId;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox CmbTipoId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtCodigoCiudad;
        private System.Windows.Forms.ComboBox CmbCiudades;
        private MiLibreria.ErrorTxtBox TxtPrimerNombre;
        private MiLibreria.ErrorTxtBox TxtPrimerApeliido;
        private System.Windows.Forms.TextBox txtSegundoNombre;
        private System.Windows.Forms.TextBox TxtSegundoApellido;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}