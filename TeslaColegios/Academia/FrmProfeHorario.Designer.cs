﻿namespace TeslaColegios.Academia
{
    partial class FrmProfeHorario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.datagridvHoras = new System.Windows.Forms.DataGridView();
            this.Hora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lunes = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Martes = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Miercoles = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Jueves = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Viernes = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Horario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ID_EMPLEADO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipo_Identificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descripcion_Tipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Numero_Identificacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre_Profesor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.datagridvHoras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(51, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Profesor:";
            // 
            // datagridvHoras
            // 
            this.datagridvHoras.AllowUserToAddRows = false;
            this.datagridvHoras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagridvHoras.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Hora,
            this.Lunes,
            this.Martes,
            this.Miercoles,
            this.Jueves,
            this.Viernes,
            this.Horario});
            this.datagridvHoras.Location = new System.Drawing.Point(41, 205);
            this.datagridvHoras.Name = "datagridvHoras";
            this.datagridvHoras.Size = new System.Drawing.Size(752, 256);
            this.datagridvHoras.TabIndex = 2;
            // 
            // Hora
            // 
            this.Hora.HeaderText = "Hora";
            this.Hora.Name = "Hora";
            // 
            // Lunes
            // 
            this.Lunes.HeaderText = "Lunes";
            this.Lunes.Name = "Lunes";
            this.Lunes.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Lunes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Martes
            // 
            this.Martes.HeaderText = "Martes";
            this.Martes.Name = "Martes";
            this.Martes.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Martes.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Miercoles
            // 
            this.Miercoles.HeaderText = "Miercoles";
            this.Miercoles.Name = "Miercoles";
            // 
            // Jueves
            // 
            this.Jueves.HeaderText = "Jueves";
            this.Jueves.Name = "Jueves";
            // 
            // Viernes
            // 
            this.Viernes.HeaderText = "Viernes";
            this.Viernes.Name = "Viernes";
            // 
            // Horario
            // 
            this.Horario.HeaderText = "Horario";
            this.Horario.Name = "Horario";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_EMPLEADO,
            this.Tipo_Identificacion,
            this.Descripcion_Tipo,
            this.Numero_Identificacion,
            this.Nombre_Profesor});
            this.dataGridView1.Location = new System.Drawing.Point(41, 41);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(752, 150);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // ID_EMPLEADO
            // 
            this.ID_EMPLEADO.HeaderText = "Id Profesor";
            this.ID_EMPLEADO.Name = "ID_EMPLEADO";
            // 
            // Tipo_Identificacion
            // 
            this.Tipo_Identificacion.HeaderText = "Tipo Id";
            this.Tipo_Identificacion.Name = "Tipo_Identificacion";
            // 
            // Descripcion_Tipo
            // 
            this.Descripcion_Tipo.HeaderText = "Desc. Tipo";
            this.Descripcion_Tipo.Name = "Descripcion_Tipo";
            // 
            // Numero_Identificacion
            // 
            this.Numero_Identificacion.HeaderText = "No. Identificacion";
            this.Numero_Identificacion.Name = "Numero_Identificacion";
            // 
            // Nombre_Profesor
            // 
            this.Nombre_Profesor.HeaderText = "Profesor";
            this.Nombre_Profesor.Name = "Nombre_Profesor";
            // 
            // FrmProfeHorario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 475);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.datagridvHoras);
            this.Controls.Add(this.label1);
            this.Name = "FrmProfeHorario";
            this.Text = "FrmProfeHorario";
            ((System.ComponentModel.ISupportInitialize)(this.datagridvHoras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView datagridvHoras;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hora;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Lunes;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Martes;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Miercoles;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Jueves;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Viernes;
        private System.Windows.Forms.DataGridViewTextBoxColumn Horario;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_EMPLEADO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo_Identificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descripcion_Tipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numero_Identificacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre_Profesor;
    }
}