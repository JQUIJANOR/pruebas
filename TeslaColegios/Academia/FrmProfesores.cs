﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MiLibreria;

namespace TeslaColegios.Academia
{
    public partial class FrmProfesores : FrmBaseMtto
    {
        Profesores AdmonProfesores = new Profesores();
        Paises AdministracionPaises = new Paises();
        Departamentos AdministracionDptos = new Departamentos();
        Ciudades AdmonCiudades = new Ciudades();
        Clases.Usu_usuarios AdmUsuarios = new Clases.Usu_usuarios();


        Boolean RegistroNuevo = false;
        Boolean bHayErrores = false;
        String Mensaje;

        public FrmProfesores()
        {
            InitializeComponent();
        }

        public void CargaComboPaises()
        {
            DataSet ds = new DataSet();
            ds = AdministracionPaises.BuscarTodos();
            ComboPais.DataSource = null; 
            ComboPais.Items.Clear();
            TxtCodigoPais.Text = "";
            TxtCodDepto.Text = "";
            TxtCodigoCiudad.Text = "";
            ComboPais.DataSource = ds.Tables[0];
            ComboPais.DisplayMember = "NOMBRE_PAIS";
            ComboPais.ValueMember = "CODIGO_PAIS";
            if (ComboPais.Items.Count != 0)
            {
                int PaisId = Convert.ToInt32(ComboPais.SelectedValue);
                TxtCodigoPais.Text = ComboPais.SelectedValue.ToString();
                CargaComboDptos(PaisId);
                CargaComboCiudades(Convert.ToInt32(CmbDepartamentos.SelectedValue));
            }
                
            

        }

        public void CargaComboDptos(int iCodigoPais)
        {
            DataSet ds = new DataSet();
            ds = AdministracionDptos.BuscarDptosPais(iCodigoPais);
            CmbDepartamentos.DataSource = null; 
            CmbDepartamentos.Items.Clear();
            TxtCodDepto.Text = "";
            TxtCodigoCiudad.Text = "";
            CmbDepartamentos.DataSource = ds.Tables[0];
            CmbDepartamentos.DisplayMember = "NOMBRE_DEPARTAMENTO";
            CmbDepartamentos.ValueMember = "CODIGO_DEPARTAMENTO";
            if (CmbDepartamentos.Items.Count != 0)
            {
                int DepartamentoId = Convert.ToInt32(ComboPais.SelectedValue);
                TxtCodDepto.Text = CmbDepartamentos.SelectedValue.ToString();
                CargaComboCiudades(DepartamentoId);
                
            }

        }

        public void CargaComboCiudades(int iCodigoDpto)
        {
            DataSet ds = new DataSet();
            ds = AdmonCiudades.BuscarCiudadesDpto(iCodigoDpto);
            CmbCiudades.DataSource = null;
            CmbCiudades.Items.Clear();
            TxtCodigoCiudad.Text = "";
            CmbCiudades.DataSource = ds.Tables[0];
            CmbCiudades.DisplayMember = "NOMBRE_MUNICIPIO";
            CmbCiudades.ValueMember = "CODIGO_MUNICIPIO";
            if (CmbCiudades.Items.Count != 0)
            {
                int Ciudadid = Convert.ToInt32(CmbCiudades.SelectedValue);
                TxtCodigoCiudad.Text = CmbCiudades.SelectedValue.ToString();
            }

        }


        private void FrmProfesores_Load(object sender, EventArgs e)
        {
            CargaComboPaises();
            CargaComboDptos(Convert.ToInt32(TxtCodigoPais.Text));
            CargaComboCiudades(Convert.ToInt32(TxtCodDepto.Text));
            CargaComboTiposId();
            dataGridView1.DataSource = bindingSource1;
            ActualizaDataGrid();

            //Formato de dataGriedView
            dataGridView1.Refresh();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

        }


        public void ActualizaDataGrid()
        {
            DataSet ds = new DataSet();
            ds = AdmonProfesores.BuscarTodos();
            //dataGridView1.DataSource = ds.Tables[0];
            bindingSource1.DataSource = ds.Tables[0];

            //Formato de dataGriedView
            dataGridView1.Refresh();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

        }

        public override void Nuevo()
        {
            Boolean bHayErrores = false;
            RegistroNuevo = true;
            bHayErrores = Utilidades.LimpiarTxtFormulario(this);

        }

        public override void Eliminar()
        {
            bHayErrores = false;
            Mensaje = "";
            try
            {
                //AdmonProfesores.Eliminar(Convert.ToInt32(TxtCodigoDpto.Text));
                ActualizaDataGrid();
            }
            catch (SqlException ex)
            {
                Mensaje = String.Format("Error al actualizar: {0}", ex.Message);
                MessageBox.Show(Mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bHayErrores = true;
            }

        }

        public override void Consultar()
        {
            bool encontro;
            string ColumnaBuscar;
            ColumnaBuscar = "";
            if (RbtCodigo.Checked)
            {
                ColumnaBuscar = "NUMERO_IDENTIFICACION";
            }
            if (RbtNombre.Checked)
            {
                ColumnaBuscar = "PRIMER_NOMBRE";
            }

            encontro = Utilidades.Buscardgv(TxtBuscado.Text, ColumnaBuscar, dataGridView1);
            if (!encontro)
            {
                MessageBox.Show("Registro no encontrado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public override bool Guardar()
        {
            errorProvider1.Clear();
            if (!(Utilidades.ValidarFormulario(this, errorProvider1) == false))
            {
                return false;
            }
            bHayErrores = false;
            Mensaje = "";
            try
            {
                if (RegistroNuevo)
                {
                    AdmonProfesores.Crear(Convert.ToDecimal(TxtIdProfesor.Text), Convert.ToInt32(TxtTipoId.Text), TxtNoIdentificacion.Text, Convert.ToInt32(TxtCodigoPais.Text), Convert.ToInt32(TxtCodDepto.Text), Convert.ToInt32(TxtCodigoCiudad.Text), TxtPrimerNombre.Text, txtSegundoNombre.Text, TxtPrimerApeliido.Text, TxtSegundoApellido.Text);
                }
                else
                {
                    AdmonProfesores.Modificar(Convert.ToDecimal(TxtIdProfesor.Text), Convert.ToInt32(TxtTipoId.Text), TxtNoIdentificacion.Text, Convert.ToInt32(TxtCodigoPais.Text), Convert.ToInt32(TxtCodDepto.Text), Convert.ToInt32(TxtCodigoCiudad.Text), TxtPrimerNombre.Text, txtSegundoNombre.Text, TxtPrimerApeliido.Text, TxtSegundoApellido.Text);
                }
                ActualizaDataGrid();

            }
            catch (SqlException ex)
            {
                Mensaje = String.Format("Error al actualizar: {0}", ex.Message);
                MessageBox.Show(Mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bHayErrores = true;

            }
            RegistroNuevo = false;
            return bHayErrores;

        }

      

        private void ComboPais_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int iPais;

            iPais = Convert.ToInt32(ComboPais.SelectedValue);
            TxtCodigoPais.Text = iPais.ToString();
            CargaComboDptos(iPais);
            CargaComboCiudades(Convert.ToInt32(CmbDepartamentos.SelectedValue));
        }

        private void CmbDepartamentos_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int iDpto;

            iDpto = Convert.ToInt32(CmbDepartamentos.SelectedValue);
            TxtCodDepto.Text = iDpto.ToString();
            CargaComboCiudades(iDpto);

        }

        public void CargaComboTiposId()
        {
            DataSet ds = new DataSet();
            ds = AdmUsuarios.BuscarTodosTiposId();
            CmbTipoId.DataSource = null;
            CmbTipoId.Items.Clear();
            TxtTipoId.Text = "";
            CmbTipoId.DataSource = ds.Tables[0];
            CmbTipoId.DisplayMember = "DESCRIPCION_TIPO";
            CmbTipoId.ValueMember = "TIPO_IDENTIFICACION";
            if (CmbTipoId.Items.Count != 0)
            {
                int TipoId = Convert.ToInt32(CmbTipoId.SelectedValue);
                TxtTipoId.Text = CmbTipoId.SelectedValue.ToString();
            }
        }

        private void CmbTipoId_SelectionChangeCommitted(object sender, EventArgs e)
        {
            int iTipoId;

            iTipoId = Convert.ToInt32(CmbTipoId.SelectedValue);
            TxtTipoId.Text = iTipoId.ToString();
            

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                TxtIdProfesor.Text = Convert.ToString(dataGridView1.SelectedRows[0].Cells[0].Value);
                TxtTipoId.Text = Convert.ToString(dataGridView1.SelectedRows[0].Cells[1].Value);
                TxtNoIdentificacion.Text = Convert.ToString(dataGridView1.SelectedRows[0].Cells[2].Value);
                //Faltan

            }
        }

       

    }

}
