﻿namespace TeslaColegios
{
    partial class FrmBaseMtto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnLast = new System.Windows.Forms.Button();
            this.BtnNext = new System.Windows.Forms.Button();
            this.BtnPrior = new System.Windows.Forms.Button();
            this.BtnFirst = new System.Windows.Forms.Button();
            this.BtnSalir = new System.Windows.Forms.Button();
            this.BtnNuevo = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnLast);
            this.panel1.Controls.Add(this.BtnNext);
            this.panel1.Controls.Add(this.BtnPrior);
            this.panel1.Controls.Add(this.BtnFirst);
            this.panel1.Controls.Add(this.BtnSalir);
            this.panel1.Controls.Add(this.BtnNuevo);
            this.panel1.Controls.Add(this.btnGuardar);
            this.panel1.Controls.Add(this.btnEliminar);
            this.panel1.Controls.Add(this.btnConsultar);
            this.panel1.Location = new System.Drawing.Point(12, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(462, 40);
            this.panel1.TabIndex = 3;
            // 
            // BtnLast
            // 
            this.BtnLast.Image = global::TeslaColegios.Properties.Resources.Last12;
            this.BtnLast.Location = new System.Drawing.Point(245, 5);
            this.BtnLast.Name = "BtnLast";
            this.BtnLast.Size = new System.Drawing.Size(32, 32);
            this.BtnLast.TabIndex = 8;
            this.BtnLast.UseVisualStyleBackColor = true;
            this.BtnLast.Click += new System.EventHandler(this.BtnLast_Click);
            // 
            // BtnNext
            // 
            this.BtnNext.Image = global::TeslaColegios.Properties.Resources.Next1;
            this.BtnNext.Location = new System.Drawing.Point(213, 5);
            this.BtnNext.Name = "BtnNext";
            this.BtnNext.Size = new System.Drawing.Size(32, 32);
            this.BtnNext.TabIndex = 7;
            this.BtnNext.UseVisualStyleBackColor = true;
            this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // BtnPrior
            // 
            this.BtnPrior.Image = global::TeslaColegios.Properties.Resources.Prior1;
            this.BtnPrior.Location = new System.Drawing.Point(181, 5);
            this.BtnPrior.Name = "BtnPrior";
            this.BtnPrior.Size = new System.Drawing.Size(32, 32);
            this.BtnPrior.TabIndex = 6;
            this.BtnPrior.UseVisualStyleBackColor = true;
            this.BtnPrior.Click += new System.EventHandler(this.BtnPrior_Click);
            // 
            // BtnFirst
            // 
            this.BtnFirst.Image = global::TeslaColegios.Properties.Resources.First11;
            this.BtnFirst.Location = new System.Drawing.Point(149, 5);
            this.BtnFirst.Name = "BtnFirst";
            this.BtnFirst.Size = new System.Drawing.Size(32, 32);
            this.BtnFirst.TabIndex = 5;
            this.BtnFirst.UseVisualStyleBackColor = true;
            this.BtnFirst.Click += new System.EventHandler(this.BtnFirst_Click);
            // 
            // BtnSalir
            // 
            this.BtnSalir.Image = global::TeslaColegios.Properties.Resources.Exit01;
            this.BtnSalir.Location = new System.Drawing.Point(299, 5);
            this.BtnSalir.Name = "BtnSalir";
            this.BtnSalir.Size = new System.Drawing.Size(32, 32);
            this.BtnSalir.TabIndex = 4;
            this.toolTip1.SetToolTip(this.BtnSalir, "Salir");
            this.BtnSalir.UseVisualStyleBackColor = true;
            this.BtnSalir.Click += new System.EventHandler(this.BtnSalir_Click);
            // 
            // BtnNuevo
            // 
            this.BtnNuevo.Image = global::TeslaColegios.Properties.Resources.New01;
            this.BtnNuevo.Location = new System.Drawing.Point(40, 5);
            this.BtnNuevo.Name = "BtnNuevo";
            this.BtnNuevo.Size = new System.Drawing.Size(32, 32);
            this.BtnNuevo.TabIndex = 3;
            this.toolTip1.SetToolTip(this.BtnNuevo, "Nuevo");
            this.BtnNuevo.UseVisualStyleBackColor = true;
            this.BtnNuevo.Click += new System.EventHandler(this.BtnNuevo_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Image = global::TeslaColegios.Properties.Resources.Guardar01;
            this.btnGuardar.Location = new System.Drawing.Point(70, 5);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(32, 32);
            this.btnGuardar.TabIndex = 1;
            this.toolTip1.SetToolTip(this.btnGuardar, "Guardar");
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Image = global::TeslaColegios.Properties.Resources.Delete01;
            this.btnEliminar.Location = new System.Drawing.Point(100, 5);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(32, 32);
            this.btnEliminar.TabIndex = 2;
            this.toolTip1.SetToolTip(this.btnEliminar, "Eliminar");
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Image = global::TeslaColegios.Properties.Resources.Consultar01;
            this.btnConsultar.Location = new System.Drawing.Point(10, 5);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(32, 32);
            this.btnConsultar.TabIndex = 0;
            this.btnConsultar.Tag = "";
            this.toolTip1.SetToolTip(this.btnConsultar, "Consultar");
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // FrmBaseMtto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 426);
            this.Controls.Add(this.panel1);
            this.Name = "FrmBaseMtto";
            this.Text = "Administracion";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.Button btnConsultar;
        public System.Windows.Forms.Button btnGuardar;
        public System.Windows.Forms.Button btnEliminar;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Button BtnSalir;
        public System.Windows.Forms.Button BtnNuevo;
        public System.Windows.Forms.ErrorProvider errorProvider1;
        public System.Windows.Forms.Button BtnLast;
        public System.Windows.Forms.Button BtnNext;
        public System.Windows.Forms.Button BtnPrior;
        public System.Windows.Forms.Button BtnFirst;
    }
}