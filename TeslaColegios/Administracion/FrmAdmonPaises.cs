﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MiLibreria;


namespace TeslaColegios
{
    public partial class FrmAdmonPaises : FrmBaseMtto
    {
        Paises AdministracionPaises = new Paises();
        Boolean RegistroNuevo = false;
        Boolean bHayErrores = false;
        String Mensaje;

        //public BindingSource bindingSource1 = new BindingSource();
                
        public FrmAdmonPaises()
        {
            InitializeComponent();
        }

        private void FrmAdmonPaises_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = bindingSource1; 
            ActualizaDataGrid();
            
        }

        public void ActualizaDataGrid()
        {
            DataSet ds = new DataSet();
            ds = AdministracionPaises.BuscarTodos();
            //dataGridView1.DataSource = ds.Tables[0];
            bindingSource1.DataSource = ds.Tables[0];

            //Formato de dataGriedView
            dataGridView1.Refresh();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

        }
        

        public override void Nuevo()
        {
            Boolean bHayErrores = false;
            RegistroNuevo = true;
            bHayErrores = Utilidades.LimpiarTxtFormulario(this);
        }

        public override bool Guardar()
        {
            errorProvider1.Clear();
            if (!(Utilidades.ValidarFormulario(this, errorProvider1) == false))
            {
                return false;
            }
            bHayErrores = false;
            Mensaje = "";
            try
            {
                if (RegistroNuevo)
                {
                    AdministracionPaises.Crear(Convert.ToInt32(TxtCodigoPais.Text), txtNombrePais.Text);
                }
                else
                {
                    AdministracionPaises.Modificar(Convert.ToInt32(TxtCodigoPais.Text), txtNombrePais.Text);
                }
                ActualizaDataGrid();

            }
            catch (SqlException ex)
            {
                Mensaje = String.Format("Error al actualizar: {0}", ex.Message);
                MessageBox.Show(Mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bHayErrores = true;

            }
            RegistroNuevo = false;
            return bHayErrores;
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0 )
            {
                TxtCodigoPais.Text = Convert.ToString(dataGridView1.SelectedRows[0].Cells[0].Value);
                txtNombrePais.Text = Convert.ToString(dataGridView1.SelectedRows[0].Cells[1].Value);
            }

        }

        public override void Eliminar()
        {
            bHayErrores = false;
            Mensaje = "";
            try
            {
                AdministracionPaises.Eliminar(Convert.ToInt32(TxtCodigoPais.Text));
                ActualizaDataGrid();
            }
            catch (SqlException ex)
            {
                Mensaje = String.Format("Error al actualizar: {0}", ex.Message);
                MessageBox.Show(Mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bHayErrores = true;
            }

        }

        public override void Consultar()
        {
            bool encontro;
            string ColumnaBuscar;
            ColumnaBuscar = "";
            if (RbtCodigo.Checked) 
            {
                ColumnaBuscar = "CODIGO_PAIS";
            }
            if (RbtNombre.Checked)
            {
                ColumnaBuscar = "NOMBRE_PAIS";
            }

            encontro = Find(TxtBuscado.Text, ColumnaBuscar, dataGridView1);
            if (!encontro)
            {
                MessageBox.Show("Registro no encontrado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            
        }
        public bool  Find(string TextoABuscar, string Columna, DataGridView grid)
        {
            bool encontrado=false;
            if (TextoABuscar==string.Empty) return false;
            if (grid.RowCount == 0 ) return false;
            grid.ClearSelection();
            if (Columna==string.Empty)
            {
                foreach (DataGridViewRow row in grid.Rows)
                {
                    foreach (DataGridViewCell cell in row.Cells )
                        if (!(cell.Value== null))
                        {
                            if (cell.Value.ToString() == TextoABuscar)
                            {
                                row.Selected = true;
                                return true;
                            }
                        }
                }
            }
            else
            {
                foreach (DataGridViewRow row in grid.Rows)
                {

                    if (!(row.Cells[Columna].Value == null))
                    {
                        if (row.Cells[Columna].Value.ToString() == TextoABuscar)
                        {
                            row.Selected = true;
                            return true;
                        }
                    }
                }
            }
            return encontrado;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

         
    }
}
