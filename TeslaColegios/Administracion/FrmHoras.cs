﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TeslaColegios
{
    public partial class FrmHoras : Form
    {
        Hor_Horas HorasSemana = new Hor_Horas();

        public FrmHoras()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            Boolean bLunes, bMartes, bMiercoles, bJueves, bViernes;
            string sHorario = "";
            for (int HoraDia = 1; HoraDia < 12; HoraDia++)
            {
                bLunes = false;
                bMartes = false;
                bMiercoles = false;
                bJueves = false;
                bViernes = false;
                sHorario = "";
                cargadias(ref bLunes, ref bMartes, ref bMiercoles, ref bJueves, ref bViernes, ref  HoraDia, ref sHorario);
                if (sHorario != "")
                {
                    dataGridView1.Rows.Add(HoraDia.ToString(), bLunes, bMartes, bMiercoles, bJueves, bViernes, sHorario);
                }

            }
        }

        public void cargadias(ref Boolean bLunes, ref Boolean bMartes, ref Boolean bMiercoles, ref Boolean bJueves, ref Boolean bViernes, ref int iDiaSemana, ref string shorario)
        {
            int iDia = iDiaSemana;

            List<Hor_Horas> listaHoras = new List<Hor_Horas>();
            listaHoras = HorasSemana.ObtieneListaHoras();

            var result = listaHoras.Where(x => x.HORA == iDia).ToList();
            bLunes = false;
            bMartes = false;
            bMiercoles = false;
            bJueves = false;
            bViernes = false;

            foreach (var item in result)
            {
                if (shorario == "")
                {
                    shorario = item.HORA_INICIAL.TimeOfDay.ToString() + "-" + item.HORA_FINAL.TimeOfDay.ToString();
                }
                if (item.DIA_SEMANA == 1)
                {
                    bLunes = true;
                }
                if (item.DIA_SEMANA == 2)
                {
                    bMartes = true;
                }
                if (item.DIA_SEMANA == 3)
                {
                    bMiercoles = true;
                }
                if (item.DIA_SEMANA == 4)
                {
                    bJueves = true;
                }
                if (item.DIA_SEMANA == 5)
                {
                    bViernes = true;
                }
            }

        }

    }
}
