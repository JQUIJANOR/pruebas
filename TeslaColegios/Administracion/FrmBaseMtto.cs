﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TeslaColegios
{
    public partial class FrmBaseMtto : Form
    {
        public BindingSource bindingSource1 = new BindingSource();
        public FrmBaseMtto()
        {
            InitializeComponent();

        }
        public virtual void Eliminar()
        {
        }
        public virtual void Nuevo()
        {
        }
        public virtual void Consultar()
        {
        }
        public virtual Boolean Guardar()
        {
            return false;
        }
        public virtual void Salir()
        {
            this.Close();
        }

        public virtual void Primero()
        {
            bindingSource1.MoveFirst();

        }
        public virtual void Ultimo()
        {
            bindingSource1.MoveLast();

        }
        public virtual void Siguiente()
        {
            bindingSource1.MoveNext();

        }
        public virtual void Anterior()
        {
            bindingSource1.MovePrevious();

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Consultar();

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Eliminar();

        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            Nuevo();

        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Salir();
        }

        private void BtnFirst_Click(object sender, EventArgs e)
        {
            Primero();
        }

        private void BtnLast_Click(object sender, EventArgs e)
        {
            Ultimo();
        }

        private void BtnNext_Click(object sender, EventArgs e)
        {
            Siguiente();
        }

        private void BtnPrior_Click(object sender, EventArgs e)
        {
            Anterior();
        }




    }
}
