﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MiLibreria;



namespace TeslaColegios
{
    public partial class FrmAdmonCiudades : FrmBaseMtto
    {
        Ciudades AdministracionCiudades = new Ciudades();
        Departamentos AdministracionDptos = new Departamentos();
        Boolean RegistroNuevo = false;
        Boolean bHayErrores = false;
        String Mensaje;

        public FrmAdmonCiudades()
        {
            InitializeComponent();
        }

        public void CargaComboDptos()
        {
            DataSet ds = new DataSet();
            ds = AdministracionDptos.BuscarTodos();
            CmbDepartamentos.DataSource = ds.Tables[0];
            CmbDepartamentos.DisplayMember = "NOMBRE_DEPARTAMENTO";
            CmbDepartamentos.ValueMember = "CODIGO_DEPARTAMENTO";
            TxtCodDepto.Text = CmbDepartamentos.SelectedValue.ToString();
            

        }

        private void FrmAdmonCiudades_Load(object sender, EventArgs e)
        {
            CargaComboDptos();
            dataGridView1.DataSource = bindingSource1;
            ActualizaDataGrid();

            //Formato de dataGriedView
            dataGridView1.Refresh();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

        }

        public void ActualizaDataGrid()
        {
            DataSet ds = new DataSet();
            ds = AdministracionCiudades.BuscarTodos();
            
            //dataGridView1.DataSource = ds.Tables[0];
            bindingSource1.DataSource = ds.Tables[0];

            //Formato de dataGriedView
            dataGridView1.Refresh();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

        }

        private void CmbDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            TxtCodDepto.Text = CmbDepartamentos.SelectedValue.ToString();

        }
        public override void Nuevo()
        {
            Boolean bHayErrores = false;
            RegistroNuevo = true;
            bHayErrores = Utilidades.LimpiarTxtFormulario(this);
        }
        public override void Eliminar()
        {
            bHayErrores = false;
            Mensaje = "";
            try
            {
                AdministracionCiudades.Eliminar(Convert.ToInt32(TxtCodDepto.Text), Convert.ToInt32(TxtCodCiudad.Text));
                ActualizaDataGrid();
            }
            catch (SqlException ex)
            {
                Mensaje = String.Format("Error al actualizar: {0}", ex.Message);
                MessageBox.Show(Mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bHayErrores = true;
            }

        }
        public override void Consultar()
        {
            bool encontro;
            string ColumnaBuscar;
            ColumnaBuscar = "";
            if (RbtCodigo.Checked)
            {
                ColumnaBuscar = "CODIGO_CIUDAD";
            }
            if (RbtNombre.Checked)
            {
                ColumnaBuscar = "NOMBRE_CIUDAD";
            }

            encontro = Utilidades.Buscardgv(TxtBuscado.Text, ColumnaBuscar, dataGridView1);
            if (!encontro)
            {
                MessageBox.Show("Registro no encontrado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        public override bool Guardar()
        {
            errorProvider1.Clear();
            if (!(Utilidades.ValidarFormulario(this, errorProvider1) == false))
            {
                return false;
            }
            bHayErrores = false;
            Mensaje = "";
            try
            {
                if (RegistroNuevo)
                {
                    AdministracionCiudades.Crear(Convert.ToInt32(TxtCodCiudad.Text), Convert.ToInt32(TxtCodDepto.Text), TxtNombreCiudad.Text);
                }
                else
                {
                    AdministracionCiudades.Modificar(Convert.ToInt32(TxtCodCiudad.Text), Convert.ToInt32(TxtCodDepto.Text), TxtNombreCiudad.Text);
                }
                ActualizaDataGrid();

            }
            catch (SqlException ex)
            {
                Mensaje = String.Format("Error al actualizar: {0}", ex.Message);
                MessageBox.Show(Mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bHayErrores = true;

            }
            RegistroNuevo = false;
            return bHayErrores;

        }
    }
}
