﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using MiLibreria;
using TeslaColegios.Clases;

namespace TeslaColegios
{
    
    public partial class FrmLogin : Form
    {
        

        public FrmLogin()
        {
            InitializeComponent();
        }
        
        public static Usu_usuarios usuario = new Usu_usuarios();

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void BtnIniciar_Click(object sender, EventArgs e)
        {
            /*
            try
            {
                
                string cadena = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                //SqlConnection cnn = new SqlConnection(@"Server=DESKTOP-IICALOM\SQLEXPRESS;Database=TslColegiosDB;Uid=sa;Pwd=grasco;");
                SqlConnection cnn = new SqlConnection(cadena);
                cnn.Open();
                MessageBox.Show("Conecto");
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error al conectar a la bd " + error.Message);

            };
              */
            try
            {
                string sValido, sMensaje, sNivelUsuario;
                Boolean bValido;
                Usu_usuarios usu = new Usu_usuarios();
                DataSet ds = usu.Validar_Usuario(TxtIdUsuario.Text.Trim(), TxtPassword.Text.Trim());

                sValido = ds.Tables[0].Rows[0]["Valido"].ToString().Trim();
                sMensaje= ds.Tables[0].Rows[0]["Respuesta"].ToString().Trim();
                sNivelUsuario = ds.Tables[0].Rows[0]["Nivel"].ToString().Trim();
                if (sValido == "True")
                {
                    MessageBox.Show(sMensaje);

                    bValido = usuario.CargaClase_Usuario(TxtIdUsuario.Text.Trim());
                    /*La clase usuario queda publica para las otras formas y se actualiza*/
                    if (int.Parse(sNivelUsuario) == (int)Listas.NivelUsuario.Administrador)
                    {
                        FrmAdmin VenAd = new FrmAdmin();
                        this.Hide();
                        VenAd.Show();

                    }
                    else
                    {
                        FrmUsuario VenUs = new FrmUsuario();
                        this.Hide();
                        VenUs.Show();

                    }
                }
                else
                {
                    MessageBox.Show("No se permite el ingreso " + sMensaje);
                }


            }
            catch (Exception error1)
            {
                MessageBox.Show("Ha ocurrido un error al conectar a la bd " + error1.Message);
            }

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FrmLogin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();


        }
    }
}
