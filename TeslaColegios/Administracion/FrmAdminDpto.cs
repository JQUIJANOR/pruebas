﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MiLibreria;


namespace TeslaColegios
{
    public partial class FrmAdminDpto : FrmBaseMtto
    {
        Paises AdministracionPaises = new Paises();
        Departamentos AdministracionDptos = new Departamentos();
        Boolean RegistroNuevo = false;
        Boolean bHayErrores = false;
        String Mensaje;

        public FrmAdminDpto()
        {
            InitializeComponent();
        }

        public void CargaComboPaises()
        {
            DataSet ds = new DataSet();
            ds = AdministracionPaises.BuscarTodos();
            ComboPais.DataSource = ds.Tables[0];
            ComboPais.DisplayMember = "NOMBRE_PAIS";
            ComboPais.ValueMember = "CODIGO_PAIS";
            TxtCodigoPais.Text = ComboPais.SelectedValue.ToString();

        }

        private void FrmAdminDpto_Load(object sender, EventArgs e)
        {
            CargaComboPaises();
            dataGridView1.DataSource = bindingSource1;
            ActualizaDataGrid();

            //Formato de dataGriedView
            dataGridView1.Refresh();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void ComboPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            TxtCodigoPais.Text = ComboPais.SelectedValue.ToString();
        }

        public void ActualizaDataGrid()
        {
            DataSet ds = new DataSet();
            ds = AdministracionDptos.BuscarTodos();
            //dataGridView1.DataSource = ds.Tables[0];
            bindingSource1.DataSource = ds.Tables[0];

            //Formato de dataGriedView
            dataGridView1.Refresh();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

        }

        public override void Nuevo()
        {
            Boolean bHayErrores = false;
            RegistroNuevo = true;
            bHayErrores = Utilidades.LimpiarTxtFormulario(this);
        }
        public override bool Guardar()
        {
            errorProvider1.Clear();
            if (!(Utilidades.ValidarFormulario(this, errorProvider1) == false))
            {
                return false;
            }
            bHayErrores = false;
            Mensaje = "";
            try
            {
                if (RegistroNuevo)
                {
                    AdministracionDptos.Crear(Convert.ToInt32(TxtCodigoDpto.Text), Convert.ToInt32(TxtCodigoPais.Text), TxtNombreDpto.Text);
                }
                else
                {
                    AdministracionDptos.Modificar(Convert.ToInt32(TxtCodigoDpto.Text), Convert.ToInt32(TxtCodigoPais.Text), TxtNombreDpto.Text);
                }
                ActualizaDataGrid();

            }
            catch (SqlException ex)
            {
                Mensaje = String.Format("Error al actualizar: {0}", ex.Message);
                MessageBox.Show(Mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bHayErrores = true;

            }
            RegistroNuevo = false;
            return bHayErrores;
        }

        

        private void dataGridView1_SelectionChanged_1(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                TxtCodigoDpto.Text = Convert.ToString(dataGridView1.SelectedRows[0].Cells[0].Value);
                TxtCodigoPais.Text = Convert.ToString(dataGridView1.SelectedRows[0].Cells[1].Value);
                TxtNombreDpto.Text = Convert.ToString(dataGridView1.SelectedRows[0].Cells[3].Value);

            }
        }

        public override void Eliminar()
        {
            bHayErrores = false;
            Mensaje = "";
            try
            {
                AdministracionDptos.Eliminar(Convert.ToInt32(TxtCodigoDpto.Text));
                ActualizaDataGrid();
            }
            catch (SqlException ex)
            {
                Mensaje = String.Format("Error al actualizar: {0}", ex.Message);
                MessageBox.Show(Mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                bHayErrores = true;
            }

        }
        public override void Consultar()
        {
            bool encontro;
            string ColumnaBuscar;
            ColumnaBuscar = "";
            if (RbtCodigo.Checked)
            {
                ColumnaBuscar = "CODIGO_DEPARTAMENTO";
            }
            if (RbtNombre.Checked)
            {
                ColumnaBuscar = "NOMBRE_DEPARTAMENTO";
            }

            encontro = Utilidades.Buscardgv(TxtBuscado.Text, ColumnaBuscar, dataGridView1);
            if (!encontro)
            {
                MessageBox.Show("Registro no encontrado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        


    }
}
