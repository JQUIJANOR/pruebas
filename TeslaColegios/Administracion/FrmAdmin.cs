﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TeslaColegios
{
    public partial class FrmAdmin : Form
    {
        public FrmAdmin()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmContenedorppal VentanaPpal = new frmContenedorppal();
            this.Hide();
            VentanaPpal.Show();

        }

        private void FrmAdmin_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FrmAdmin_Load(object sender, EventArgs e)
        {
            lblUsuario.Text = FrmLogin.usuario.Id_Usuario;
            LblIde.Text = FrmLogin.usuario.Identificacion.ToString();
            LblNombre.Text = FrmLogin.usuario.Nombre;



        }
    }
}
