﻿namespace TeslaColegios
{
    partial class FrmAdminDpto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.RbtNombre = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.RbtCodigo = new System.Windows.Forms.RadioButton();
            this.TxtBuscado = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TxtCodigoPais = new System.Windows.Forms.TextBox();
            this.ComboPais = new System.Windows.Forms.ComboBox();
            this.TxtNombreDpto = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtCodigoDpto = new MiLibreria.ErrorTxtBox();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 52);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(572, 150);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged_1);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.RbtNombre);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.RbtCodigo);
            this.groupBox2.Controls.Add(this.TxtBuscado);
            this.groupBox2.Location = new System.Drawing.Point(22, 208);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(563, 38);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            // 
            // RbtNombre
            // 
            this.RbtNombre.AutoSize = true;
            this.RbtNombre.Location = new System.Drawing.Point(148, 15);
            this.RbtNombre.Name = "RbtNombre";
            this.RbtNombre.Size = new System.Drawing.Size(88, 17);
            this.RbtNombre.TabIndex = 1;
            this.RbtNombre.TabStop = true;
            this.RbtNombre.Text = "Nombre Dpto";
            this.RbtNombre.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Buscar:";
            // 
            // RbtCodigo
            // 
            this.RbtCodigo.AutoSize = true;
            this.RbtCodigo.Location = new System.Drawing.Point(58, 14);
            this.RbtCodigo.Name = "RbtCodigo";
            this.RbtCodigo.Size = new System.Drawing.Size(84, 17);
            this.RbtCodigo.TabIndex = 0;
            this.RbtCodigo.TabStop = true;
            this.RbtCodigo.Text = "Codigo Dpto";
            this.RbtCodigo.UseVisualStyleBackColor = true;
            // 
            // TxtBuscado
            // 
            this.TxtBuscado.Location = new System.Drawing.Point(354, 12);
            this.TxtBuscado.Name = "TxtBuscado";
            this.TxtBuscado.Size = new System.Drawing.Size(182, 20);
            this.TxtBuscado.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TxtCodigoPais);
            this.groupBox1.Controls.Add(this.ComboPais);
            this.groupBox1.Controls.Add(this.TxtNombreDpto);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TxtCodigoDpto);
            this.groupBox1.Location = new System.Drawing.Point(27, 253);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(596, 139);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // TxtCodigoPais
            // 
            this.TxtCodigoPais.Enabled = false;
            this.TxtCodigoPais.Location = new System.Drawing.Point(145, 36);
            this.TxtCodigoPais.Name = "TxtCodigoPais";
            this.TxtCodigoPais.Size = new System.Drawing.Size(62, 20);
            this.TxtCodigoPais.TabIndex = 0;
            // 
            // ComboPais
            // 
            this.ComboPais.FormattingEnabled = true;
            this.ComboPais.Location = new System.Drawing.Point(215, 35);
            this.ComboPais.Name = "ComboPais";
            this.ComboPais.Size = new System.Drawing.Size(170, 21);
            this.ComboPais.TabIndex = 5;
            this.ComboPais.SelectedIndexChanged += new System.EventHandler(this.ComboPais_SelectedIndexChanged);
            // 
            // TxtNombreDpto
            // 
            this.TxtNombreDpto.Location = new System.Drawing.Point(145, 100);
            this.TxtNombreDpto.Name = "TxtNombreDpto";
            this.TxtNombreDpto.Size = new System.Drawing.Size(271, 20);
            this.TxtNombreDpto.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 107);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nombre Departamento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Codigo Departamento";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pais";
            // 
            // TxtCodigoDpto
            // 
            this.TxtCodigoDpto.Location = new System.Drawing.Point(145, 68);
            this.TxtCodigoDpto.Name = "TxtCodigoDpto";
            this.TxtCodigoDpto.Size = new System.Drawing.Size(100, 20);
            this.TxtCodigoDpto.TabIndex = 0;
            this.TxtCodigoDpto.Validar = true;
            // 
            // FrmAdminDpto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 426);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dataGridView1);
            this.Name = "FrmAdminDpto";
            this.Text = "FrmAdminDpto";
            this.Load += new System.EventHandler(this.FrmAdminDpto_Load);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.dataGridView1, 0);
            this.Controls.SetChildIndex(this.groupBox2, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton RbtNombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton RbtCodigo;
        private System.Windows.Forms.TextBox TxtBuscado;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ComboPais;
        private System.Windows.Forms.TextBox TxtNombreDpto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private MiLibreria.ErrorTxtBox TxtCodigoDpto;
        private System.Windows.Forms.TextBox TxtCodigoPais;
    }
}