/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     3/12/2018 9:06:58 p. m.                      */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ADM_BARRIOS') and o.name = 'FK_ADM_BARR_REFERENCE_ADM_MUNI')
alter table ADM_BARRIOS
   drop constraint FK_ADM_BARR_REFERENCE_ADM_MUNI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ADM_DEPARTAMENTO') and o.name = 'FK_ADM_DEPA_REFERENCE_ADM_PAIS')
alter table ADM_DEPARTAMENTO
   drop constraint FK_ADM_DEPA_REFERENCE_ADM_PAIS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ADM_MUNICIPIOS') and o.name = 'FK_ADM_MUNI_REFERENCE_ADM_DEPA')
alter table ADM_MUNICIPIOS
   drop constraint FK_ADM_MUNI_REFERENCE_ADM_DEPA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ALU_ALUMNOS') and o.name = 'FK_ALU_ALUM_REFERENCE_USU_TIPO')
alter table ALU_ALUMNOS
   drop constraint FK_ALU_ALUM_REFERENCE_USU_TIPO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ALU_ALUMNOS') and o.name = 'FK_ALU_ALUM_REFERENCE_ADM_PAIS')
alter table ALU_ALUMNOS
   drop constraint FK_ALU_ALUM_REFERENCE_ADM_PAIS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ALU_ALUMNOS') and o.name = 'FK_ALU_ALUM_REFERENCE_ADM_DEPA')
alter table ALU_ALUMNOS
   drop constraint FK_ALU_ALUM_REFERENCE_ADM_DEPA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ALU_ALUMNOS') and o.name = 'FK_ALU_ALUM_REFERENCE_ADM_MUNI')
alter table ALU_ALUMNOS
   drop constraint FK_ALU_ALUM_REFERENCE_ADM_MUNI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CARGOS') and o.name = 'FK_CARGOS_REFERENCE_CARGOS')
alter table CARGOS
   drop constraint FK_CARGOS_REFERENCE_CARGOS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('EMPLEADOS') and o.name = 'FK_EMPLEADO_REFERENCE_CARGOS')
alter table EMPLEADOS
   drop constraint FK_EMPLEADO_REFERENCE_CARGOS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('EMPLEADOS') and o.name = 'FK_EMPLEADO_REFERENCE_PROFESIO')
alter table EMPLEADOS
   drop constraint FK_EMPLEADO_REFERENCE_PROFESIO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('EMPLEADOS') and o.name = 'FK_EMPLEADO_REFERENCE_ADM_BARR')
alter table EMPLEADOS
   drop constraint FK_EMPLEADO_REFERENCE_ADM_BARR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('EMPLEADOS') and o.name = 'FK_EMPLEADO_REFERENCE_ADM_MUNI')
alter table EMPLEADOS
   drop constraint FK_EMPLEADO_REFERENCE_ADM_MUNI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('EMPLEADOS') and o.name = 'FK_EMPLEADO_REFERENCE_USU_TIPO')
alter table EMPLEADOS
   drop constraint FK_EMPLEADO_REFERENCE_USU_TIPO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('HOR_CURSOS') and o.name = 'FK_HOR_CURS_REFERENCE_PRO_PROF')
alter table HOR_CURSOS
   drop constraint FK_HOR_CURS_REFERENCE_PRO_PROF
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('HOR_CURSO_ASIGNATURAS') and o.name = 'FK_HOR_CURS_REFERENCE_HOR_CURS')
alter table HOR_CURSO_ASIGNATURAS
   drop constraint FK_HOR_CURS_REFERENCE_HOR_CURS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('HOR_CURSO_ASIGNATURAS') and o.name = 'FK_HOR_CURS_REFERENCE_ASIGNATU')
alter table HOR_CURSO_ASIGNATURAS
   drop constraint FK_HOR_CURS_REFERENCE_ASIGNATU
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('HOR_HORARIOS_CLASES') and o.name = 'FK_HOR_HORA_REFERENCE_HOR_HORA')
alter table HOR_HORARIOS_CLASES
   drop constraint FK_HOR_HORA_REFERENCE_HOR_HORA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('HOR_HORARIOS_CLASES') and o.name = 'FK_HOR_HORA_REFERENCE_ASIGNATU')
alter table HOR_HORARIOS_CLASES
   drop constraint FK_HOR_HORA_REFERENCE_ASIGNATU
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('HOR_HORARIOS_CLASES') and o.name = 'FK_HOR_HORA_REFERENCE_PRO_PROF')
alter table HOR_HORARIOS_CLASES
   drop constraint FK_HOR_HORA_REFERENCE_PRO_PROF
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('HOR_HORARIOS_CLASES') and o.name = 'FK_HOR_HORA_REFERENCE_HOR_CURS')
alter table HOR_HORARIOS_CLASES
   drop constraint FK_HOR_HORA_REFERENCE_HOR_CURS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('HOR_PROFESOR_HORA') and o.name = 'FK_HOR_PROF_REFERENCE_PRO_PROF')
alter table HOR_PROFESOR_HORA
   drop constraint FK_HOR_PROF_REFERENCE_PRO_PROF
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('HOR_PROFESOR_HORA') and o.name = 'FK_HOR_PROF_REFERENCE_HOR_HORA')
alter table HOR_PROFESOR_HORA
   drop constraint FK_HOR_PROF_REFERENCE_HOR_HORA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('INSTITUCIONES') and o.name = 'FK_INSTITUC_REFERENCE_ADM_MUNI')
alter table INSTITUCIONES
   drop constraint FK_INSTITUC_REFERENCE_ADM_MUNI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('INS_SEDES') and o.name = 'FK_INS_SEDE_REFERENCE_INSTITUC')
alter table INS_SEDES
   drop constraint FK_INS_SEDE_REFERENCE_INSTITUC
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('INS_SEDES') and o.name = 'FK_INS_SEDE_REFERENCE_ADM_BARR')
alter table INS_SEDES
   drop constraint FK_INS_SEDE_REFERENCE_ADM_BARR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('INS_SEDES') and o.name = 'FK_INS_SEDE_REFERENCE_EMPLEA1IDX')
alter table INS_SEDES
   drop constraint FK_INS_SEDE_REFERENCE_EMPLEA1IDX
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('INS_SEDES') and o.name = 'FK_INS_SEDE_REFERENCE_EMPLEADO')
alter table INS_SEDES
   drop constraint FK_INS_SEDE_REFERENCE_EMPLEADO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PROFESIONES') and o.name = 'FK_PROFESIO_REFERENCE_NIVELES_')
alter table PROFESIONES
   drop constraint FK_PROFESIO_REFERENCE_NIVELES_
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRO_PROFESORES') and o.name = 'FK_PRO_PROF_REFERENCE_ADM_DEPA')
alter table PRO_PROFESORES
   drop constraint FK_PRO_PROF_REFERENCE_ADM_DEPA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRO_PROFESORES') and o.name = 'FK_PRO_PROF_REFERENCE_ADM_MUNI')
alter table PRO_PROFESORES
   drop constraint FK_PRO_PROF_REFERENCE_ADM_MUNI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRO_PROFESORES') and o.name = 'FK_PRO_PROF_REFERENCE_EMPLEADO')
alter table PRO_PROFESORES
   drop constraint FK_PRO_PROF_REFERENCE_EMPLEADO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRO_PROFESORES') and o.name = 'FK_PRO_PROF_REFERENCE_USU_TIPO')
alter table PRO_PROFESORES
   drop constraint FK_PRO_PROF_REFERENCE_USU_TIPO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PRO_PROFESORES') and o.name = 'FK_PRO_PROF_REFERENCE_ADM_PAIS')
alter table PRO_PROFESORES
   drop constraint FK_PRO_PROF_REFERENCE_ADM_PAIS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('USU_USUARIOS') and o.name = 'FK_USU_USUA_REFERENCE_USU_TIPO')
alter table USU_USUARIOS
   drop constraint FK_USU_USUA_REFERENCE_USU_TIPO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ADM_BARRIOS')
            and   type = 'U')
   drop table ADM_BARRIOS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ADM_DEPARTAMENTO')
            and   type = 'U')
   drop table ADM_DEPARTAMENTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ADM_MUNICIPIOS')
            and   type = 'U')
   drop table ADM_MUNICIPIOS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ADM_PAIS')
            and   type = 'U')
   drop table ADM_PAIS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ALU_ALUMNOS')
            and   type = 'U')
   drop table ALU_ALUMNOS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ASIGNATURAS')
            and   type = 'U')
   drop table ASIGNATURAS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CARGOS')
            and   type = 'U')
   drop table CARGOS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EMPLEADOS')
            and   type = 'U')
   drop table EMPLEADOS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('HOR_CURSOS')
            and   type = 'U')
   drop table HOR_CURSOS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('HOR_CURSO_ASIGNATURAS')
            and   type = 'U')
   drop table HOR_CURSO_ASIGNATURAS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('HOR_HORARIOS_CLASES')
            and   type = 'U')
   drop table HOR_HORARIOS_CLASES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('HOR_HORAS')
            and   type = 'U')
   drop table HOR_HORAS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('HOR_PROFESOR_HORA')
            and   type = 'U')
   drop table HOR_PROFESOR_HORA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('INSTITUCIONES')
            and   type = 'U')
   drop table INSTITUCIONES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('INS_SEDES')
            and   type = 'U')
   drop table INS_SEDES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('NIVELES_EDUCACION')
            and   type = 'U')
   drop table NIVELES_EDUCACION
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PROFESIONES')
            and   type = 'U')
   drop table PROFESIONES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PRO_PROFESORES')
            and   type = 'U')
   drop table PRO_PROFESORES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('USU_TIPOS_IDENTIFICACION')
            and   type = 'U')
   drop table USU_TIPOS_IDENTIFICACION
go

if exists (select 1
            from  sysobjects
           where  id = object_id('USU_USUARIOS')
            and   type = 'U')
   drop table USU_USUARIOS
go

/*==============================================================*/
/* Table: ADM_BARRIOS                                           */
/*==============================================================*/
create table ADM_BARRIOS (
   CODIGO_DEPARTAMENTO  int                  not null,
   CODIGO_MUNICIPIO     int                  not null,
   CODIGO_BARRIO        int                  not null,
   NOMBRE_BARRIO        varchar(50)          null,
   constraint PK_ADM_BARRIOS primary key (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO, CODIGO_BARRIO)
)
go

/*==============================================================*/
/* Table: ADM_DEPARTAMENTO                                      */
/*==============================================================*/
create table ADM_DEPARTAMENTO (
   CODIGO_DEPARTAMENTO  int                  not null,
   CODIGO_PAIS          int                  not null,
   NOMBRE_DEPARTAMENTO  varchar(80)          null,
   constraint PK_ADM_DEPARTAMENTO primary key (CODIGO_DEPARTAMENTO)
)
go

/*==============================================================*/
/* Table: ADM_MUNICIPIOS                                        */
/*==============================================================*/
create table ADM_MUNICIPIOS (
   CODIGO_DEPARTAMENTO  int                  not null,
   CODIGO_MUNICIPIO     int                  not null,
   NOMBRE_MUNICIPIO     varchar(80)          null,
   constraint PK_ADM_MUNICIPIOS primary key (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO)
)
go

/*==============================================================*/
/* Table: ADM_PAIS                                              */
/*==============================================================*/
create table ADM_PAIS (
   CODIGO_PAIS          int                  not null,
   NOMBRE_PAIS          varchar(80)          null,
   constraint PK_ADM_PAIS primary key (CODIGO_PAIS)
)
go

/*==============================================================*/
/* Table: ALU_ALUMNOS                                           */
/*==============================================================*/
create table ALU_ALUMNOS (
   TIPO_IDENTIFICACION  smallint             not null,
   NUMERO_IDENTIFICACION decimal(15,0)        not null,
   CODIGO_PAIS          int                  null,
   CODIGO_DEPARTAMENTO  int                  null,
   CODIGO_MUNICIPIO     int                  null,
   PRIMER_NOMBRE        varchar(80)          null,
   SEGUNDO_NOMBRE       varchar(80)          null,
   PRIMER_APELLIDO      varchar(80)          null,
   SEGUNDO_APELLIDO     varchar(80)          null,
   constraint PK_ALU_ALUMNOS primary key (TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION)
)
go

/*==============================================================*/
/* Table: ASIGNATURAS                                           */
/*==============================================================*/
create table ASIGNATURAS (
   ID_ASIGNATURA        int                  not null,
   NOMBRE_ASIGNATURA    varchar(60)          null,
   HORAS_SEMANALES      int                  null,
   constraint PK_ASIGNATURAS primary key (ID_ASIGNATURA)
)
go

/*==============================================================*/
/* Table: CARGOS                                                */
/*==============================================================*/
create table CARGOS (
   CODIGO_CARGO         int                  not null,
   NOMBRE_CARGO         varchar(60)          null,
   DESCRIPCION          varchar(3000)        null,
   CODIGO_DEPENDIENTE   int                  null,
   constraint PK_CARGOS primary key (CODIGO_CARGO)
)
go

/*==============================================================*/
/* Table: EMPLEADOS                                             */
/*==============================================================*/
create table EMPLEADOS (
   ID_EMPLEADO          decimal(15,0)        not null,
   TIPO_IDENTIFICACION  smallint             null,
   NO_IDENTIFICACION    decimal(15,0)        null,
   CODIGO_CARGO         int                  null,
   CODIGO_PROFESION     int                  null,
   CODIGO_DEPARTAMENTO_RESIDENCIA int                  null,
   CODIGO_MUNICIPIO_RESIDENCIA int                  null,
   CODIGO_BARRIO_RESIDENCIA int                  null,
   DIRECCION_RESIDENCIA varchar(60)          null,
   CODIGO_DEPARTAMENTO_NACIMIENTO int                  null,
   CODIGO_MUNICIPIO_NACIMIENTO int                  null,
   FECHA_NACIMIENTO     datetime             null,
   SEXO                 varchar(1)           null,
   ESTADO_CIVIL         int                  null,
   TELEFONO             varchar(20)          null,
   CORREO_ELECTRONICO   varchar(30)          null,
   constraint PK_EMPLEADOS primary key (ID_EMPLEADO)
)
go

/*==============================================================*/
/* Table: HOR_CURSOS                                            */
/*==============================================================*/
create table HOR_CURSOS (
   CODIGO_CURSO         char(6)              not null,
   ID_EMPLEADO          decimal(15,0)        null,
   TIPO_IDENTIFICACION  smallint             null,
   NUMERO_IDENTIFICACION varchar(20)          null,
   GRADO                int                  null,
   GRUPO                varchar(6)           null,
   constraint PK_HOR_CURSOS primary key (CODIGO_CURSO)
)
go

/*==============================================================*/
/* Table: HOR_CURSO_ASIGNATURAS                                 */
/*==============================================================*/
create table HOR_CURSO_ASIGNATURAS (
   CODIGO_CURSO         char(6)              not null,
   ID_ASIGNATURA        int                  not null,
   constraint PK_HOR_CURSO_ASIGNATURAS primary key (CODIGO_CURSO, ID_ASIGNATURA)
)
go

/*==============================================================*/
/* Table: HOR_HORARIOS_CLASES                                   */
/*==============================================================*/
create table HOR_HORARIOS_CLASES (
   CODIGO_CURSO         char(6)              null,
   ID_ASIGNATURA        int                  null,
   DIA_SEMAN            int                  null,
   HORA                 int                  null,
   ID_EMPLEADO          decimal(15,0)        null,
   TIPO_IDENTIFICACION  smallint             null,
   NUMERO_IDENTIFICACION varchar(20)          null
)
go

/*==============================================================*/
/* Table: HOR_HORAS                                             */
/*==============================================================*/
create table HOR_HORAS (
   DIA_SEMANA           int                  not null,
   HORA                 int                  not null,
   HORA_INICIAL         datetime             null,
   HORA_FINAL           datetime             null,
   NOMBRE_DIA           varchar(10)          null,
   constraint PK_HOR_HORAS primary key (DIA_SEMANA, HORA)
)
go

/*==============================================================*/
/* Table: HOR_PROFESOR_HORA                                     */
/*==============================================================*/
create table HOR_PROFESOR_HORA (
   ID_PROFESOR          char(10)             not null,
   DIA_SEMANA           int                  not null,
   HORA                 int                  not null,
   ID_EMPLEADO          decimal(15,0)        null,
   TIPO_IDENTIFICACION  smallint             null,
   NUMERO_IDENTIFICACION varchar(20)          null,
   DISPONIBLE           bit                  null,
   constraint PK_HOR_PROFESOR_HORA primary key (ID_PROFESOR, DIA_SEMANA, HORA)
)
go

/*==============================================================*/
/* Table: INSTITUCIONES                                         */
/*==============================================================*/
create table INSTITUCIONES (
   CODIGO_DANE_PPAL     varchar(20)          not null,
   NOMBRE_INSTITUCION   varchar(60)          null,
   SECRETARIA           varchar(20)          null,
   CODIGO_DEPARTAMENTO  int                  null,
   CODIGO_MUNICIPIO     int                  null,
   DIRECCION            varchar(60)          null,
   TELEFONO1            varchar(20)          null,
   TELEFONO2            varchar(20)          null,
   EMAIL                varchar(20)          null,
   constraint PK_INSTITUCIONES primary key (CODIGO_DANE_PPAL)
)
go

/*==============================================================*/
/* Table: INS_SEDES                                             */
/*==============================================================*/
create table INS_SEDES (
   CODIGO_DANE_PPAL     varchar(20)          not null,
   CODIGO_DANE_SEDE     varchar(20)          not null,
   CODIGO_DEPARTAMENTO  int                  null,
   CODIGO_MUNICIPIO     int                  null,
   CODIGO_BARRIO        int                  null,
   NOMBRE_SEDE          varchar(60)          null,
   DIRECCION            varchar(60)          null,
   EMAIL                varchar(20)          null,
   TELEFONO1            varchar(20)          null,
   TELEFONO2            varchar(20)          null,
   ID_RECTOR            decimal(15,0)        null,
   ID_SECRETARIA        decimal(15,0)        null,
   constraint PK_INS_SEDES primary key (CODIGO_DANE_PPAL, CODIGO_DANE_SEDE)
)
go

/*==============================================================*/
/* Table: NIVELES_EDUCACION                                     */
/*==============================================================*/
create table NIVELES_EDUCACION (
   ID_NIVEL             int                  not null,
   NIVEL_EDUCACION      char(60)             null,
   CALIFICACION         int                  null,
   constraint PK_NIVELES_EDUCACION primary key (ID_NIVEL)
)
go

/*==============================================================*/
/* Table: PROFESIONES                                           */
/*==============================================================*/
create table PROFESIONES (
   CODIGO_PROFESION     int                  not null,
   NOMBRE_PROFESION     varchar(60)          null,
   ID_NIVEL             int                  null,
   constraint PK_PROFESIONES primary key (CODIGO_PROFESION)
)
go

/*==============================================================*/
/* Table: PRO_PROFESORES                                        */
/*==============================================================*/
create table PRO_PROFESORES (
   ID_EMPLEADO          decimal(15,0)        not null,
   TIPO_IDENTIFICACION  smallint             not null,
   NUMERO_IDENTIFICACION varchar(20)          not null,
   CODIGO_PAIS          int                  null,
   CODIGO_DEPARTAMENTO  int                  null,
   CODIGO_MUNICIPIO     int                  null,
   PRIMER_NOMBRE        varchar(60)          null,
   SEGUNDO_NOMBRE       varchar(60)          null,
   PRIMER_APELLIDO      varchar(60)          null,
   SEGUNDO_APELLIDO     varchar(60)          null,
   constraint PK_PRO_PROFESORES primary key (ID_EMPLEADO, TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION),
   constraint AK_KEY_2_PRO_PROF unique (ID_EMPLEADO, TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION)
)
go

/*==============================================================*/
/* Table: USU_TIPOS_IDENTIFICACION                              */
/*==============================================================*/
create table USU_TIPOS_IDENTIFICACION (
   TIPO_IDENTIFICACION  smallint             not null,
   DESCRIPCION_TIPO     varchar(40)          null,
   constraint PK_USU_TIPOS_IDENTIFICACION primary key (TIPO_IDENTIFICACION)
)
go

/*==============================================================*/
/* Table: USU_USUARIOS                                          */
/*==============================================================*/
create table USU_USUARIOS (
   ID_USUARIO           varchar(20)          not null,
   TIPO_IDENTIFICACION  smallint             null,
   IDENTIFICACION       decimal(15,0)        null,
   NOMBRE               varchar(100)         null,
   NIVEL_USUARIO        int                  null,
   PASSWORD             varbinary(Max)       null,
   constraint PK_USU_USUARIOS primary key (ID_USUARIO)
)
go

alter table ADM_BARRIOS
   add constraint FK_ADM_BARR_REFERENCE_ADM_MUNI foreign key (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO)
      references ADM_MUNICIPIOS (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO)
go

alter table ADM_DEPARTAMENTO
   add constraint FK_ADM_DEPA_REFERENCE_ADM_PAIS foreign key (CODIGO_PAIS)
      references ADM_PAIS (CODIGO_PAIS)
go

alter table ADM_MUNICIPIOS
   add constraint FK_ADM_MUNI_REFERENCE_ADM_DEPA foreign key (CODIGO_DEPARTAMENTO)
      references ADM_DEPARTAMENTO (CODIGO_DEPARTAMENTO)
go

alter table ALU_ALUMNOS
   add constraint FK_ALU_ALUM_REFERENCE_USU_TIPO foreign key (TIPO_IDENTIFICACION)
      references USU_TIPOS_IDENTIFICACION (TIPO_IDENTIFICACION)
go

alter table ALU_ALUMNOS
   add constraint FK_ALU_ALUM_REFERENCE_ADM_PAIS foreign key (CODIGO_PAIS)
      references ADM_PAIS (CODIGO_PAIS)
go

alter table ALU_ALUMNOS
   add constraint FK_ALU_ALUM_REFERENCE_ADM_DEPA foreign key (CODIGO_DEPARTAMENTO)
      references ADM_DEPARTAMENTO (CODIGO_DEPARTAMENTO)
go

alter table ALU_ALUMNOS
   add constraint FK_ALU_ALUM_REFERENCE_ADM_MUNI foreign key (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO)
      references ADM_MUNICIPIOS (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO)
go

alter table CARGOS
   add constraint FK_CARGOS_REFERENCE_CARGOS foreign key (CODIGO_DEPENDIENTE)
      references CARGOS (CODIGO_CARGO)
go

alter table EMPLEADOS
   add constraint FK_EMPLEADO_REFERENCE_CARGOS foreign key (CODIGO_CARGO)
      references CARGOS (CODIGO_CARGO)
go

alter table EMPLEADOS
   add constraint FK_EMPLEADO_REFERENCE_PROFESIO foreign key (CODIGO_PROFESION)
      references PROFESIONES (CODIGO_PROFESION)
go

alter table EMPLEADOS
   add constraint FK_EMPLEADO_REFERENCE_ADM_BARR foreign key (CODIGO_DEPARTAMENTO_RESIDENCIA, CODIGO_MUNICIPIO_RESIDENCIA, CODIGO_BARRIO_RESIDENCIA)
      references ADM_BARRIOS (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO, CODIGO_BARRIO)
go

alter table EMPLEADOS
   add constraint FK_EMPLEADO_REFERENCE_ADM_MUNI foreign key (CODIGO_DEPARTAMENTO_NACIMIENTO, CODIGO_MUNICIPIO_NACIMIENTO)
      references ADM_MUNICIPIOS (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO)
go

alter table EMPLEADOS
   add constraint FK_EMPLEADO_REFERENCE_USU_TIPO foreign key (TIPO_IDENTIFICACION)
      references USU_TIPOS_IDENTIFICACION (TIPO_IDENTIFICACION)
go

alter table HOR_CURSOS
   add constraint FK_HOR_CURS_REFERENCE_PRO_PROF foreign key (ID_EMPLEADO, TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION)
      references PRO_PROFESORES (ID_EMPLEADO, TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION)
go

alter table HOR_CURSO_ASIGNATURAS
   add constraint FK_HOR_CURS_REFERENCE_HOR_CURS foreign key (CODIGO_CURSO)
      references HOR_CURSOS (CODIGO_CURSO)
go

alter table HOR_CURSO_ASIGNATURAS
   add constraint FK_HOR_CURS_REFERENCE_ASIGNATU foreign key (ID_ASIGNATURA)
      references ASIGNATURAS (ID_ASIGNATURA)
go

alter table HOR_HORARIOS_CLASES
   add constraint FK_HOR_HORA_REFERENCE_HOR_HORA foreign key (DIA_SEMAN, HORA)
      references HOR_HORAS (DIA_SEMANA, HORA)
go

alter table HOR_HORARIOS_CLASES
   add constraint FK_HOR_HORA_REFERENCE_ASIGNATU foreign key (ID_ASIGNATURA)
      references ASIGNATURAS (ID_ASIGNATURA)
go

alter table HOR_HORARIOS_CLASES
   add constraint FK_HOR_HORA_REFERENCE_PRO_PROF foreign key (ID_EMPLEADO, TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION)
      references PRO_PROFESORES (ID_EMPLEADO, TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION)
go

alter table HOR_HORARIOS_CLASES
   add constraint FK_HOR_HORA_REFERENCE_HOR_CURS foreign key (CODIGO_CURSO, ID_ASIGNATURA)
      references HOR_CURSO_ASIGNATURAS (CODIGO_CURSO, ID_ASIGNATURA)
go

alter table HOR_PROFESOR_HORA
   add constraint FK_HOR_PROF_REFERENCE_PRO_PROF foreign key (ID_EMPLEADO, TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION)
      references PRO_PROFESORES (ID_EMPLEADO, TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION)
go

alter table HOR_PROFESOR_HORA
   add constraint FK_HOR_PROF_REFERENCE_HOR_HORA foreign key (DIA_SEMANA, HORA)
      references HOR_HORAS (DIA_SEMANA, HORA)
go

alter table INSTITUCIONES
   add constraint FK_INSTITUC_REFERENCE_ADM_MUNI foreign key (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO)
      references ADM_MUNICIPIOS (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO)
go

alter table INS_SEDES
   add constraint FK_INS_SEDE_REFERENCE_INSTITUC foreign key (CODIGO_DANE_PPAL)
      references INSTITUCIONES (CODIGO_DANE_PPAL)
go

alter table INS_SEDES
   add constraint FK_INS_SEDE_REFERENCE_ADM_BARR foreign key (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO, CODIGO_BARRIO)
      references ADM_BARRIOS (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO, CODIGO_BARRIO)
go

alter table INS_SEDES
   add constraint FK_INS_SEDE_REFERENCE_EMPLEA1IDX foreign key (ID_RECTOR)
      references EMPLEADOS (ID_EMPLEADO)
go

alter table INS_SEDES
   add constraint FK_INS_SEDE_REFERENCE_EMPLEADO foreign key (ID_SECRETARIA)
      references EMPLEADOS (ID_EMPLEADO)
go

alter table PROFESIONES
   add constraint FK_PROFESIO_REFERENCE_NIVELES_ foreign key (ID_NIVEL)
      references NIVELES_EDUCACION (ID_NIVEL)
go

alter table PRO_PROFESORES
   add constraint FK_PRO_PROF_REFERENCE_ADM_DEPA foreign key (CODIGO_DEPARTAMENTO)
      references ADM_DEPARTAMENTO (CODIGO_DEPARTAMENTO)
go

alter table PRO_PROFESORES
   add constraint FK_PRO_PROF_REFERENCE_ADM_MUNI foreign key (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO)
      references ADM_MUNICIPIOS (CODIGO_DEPARTAMENTO, CODIGO_MUNICIPIO)
go

alter table PRO_PROFESORES
   add constraint FK_PRO_PROF_REFERENCE_EMPLEADO foreign key (ID_EMPLEADO)
      references EMPLEADOS (ID_EMPLEADO)
go

alter table PRO_PROFESORES
   add constraint FK_PRO_PROF_REFERENCE_USU_TIPO foreign key (TIPO_IDENTIFICACION)
      references USU_TIPOS_IDENTIFICACION (TIPO_IDENTIFICACION)
go

alter table PRO_PROFESORES
   add constraint FK_PRO_PROF_REFERENCE_ADM_PAIS foreign key (CODIGO_PAIS)
      references ADM_PAIS (CODIGO_PAIS)
go

alter table USU_USUARIOS
   add constraint FK_USU_USUA_REFERENCE_USU_TIPO foreign key (TIPO_IDENTIFICACION)
      references USU_TIPOS_IDENTIFICACION (TIPO_IDENTIFICACION)
go

