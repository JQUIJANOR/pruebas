using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

public class Adm_departamento
{
	cnn dbm = new cnn();

	private int CODIGO_DEPARTAMENTO;
	private int CODIGO_PAIS;
	private string NOMBRE_DEPARTAMENTO;

	public int CODIGO_DEPARTAMENTO
	{
		get { return CODIGO_DEPARTAMENTO; }
		set { CODIGO_DEPARTAMENTO = value; }
	}
	public int CODIGO_PAIS
	{
		get { return CODIGO_PAIS; }
		set { CODIGO_PAIS = value; }
	}
	public string NOMBRE_DEPARTAMENTO
	{
		get { return NOMBRE_DEPARTAMENTO; }
		set { NOMBRE_DEPARTAMENTO = value; }
	}

	public Adm_departamento(){ }

	public Boolean Crear(int vCODIGO_DEPARTAMENTO ,int vCODIGO_PAIS ,string vNOMBRE_DEPARTAMENTO)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spAdm_departamento_N", con);
			parametros = cmd.Parameters.Add("@CODIGO_DEPARTAMENTO", SqlDbType.Int);
			parametros.Value = vCODIGO_DEPARTAMENTO;
			parametros = cmd.Parameters.Add("@CODIGO_PAIS", SqlDbType.Int);
			parametros.Value = vCODIGO_PAIS;
			parametros = cmd.Parameters.Add("@NOMBRE_DEPARTAMENTO", SqlDbType.VarChar);
			parametros.Value = vNOMBRE_DEPARTAMENTO;
			cmd.CommandType = CommandType.StoredProcedure;
			if (cmd.ExecuteNonQuery() > 0)
				res = true;
			con.Close();
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return res;
	}


	public Boolean Modificar(int vCODIGO_DEPARTAMENTO ,int vCODIGO_PAIS ,string vNOMBRE_DEPARTAMENTO)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spAdm_departamento_M", con);
			parametros = cmd.Parameters.Add("@CODIGO_DEPARTAMENTO", SqlDbType.Int);
			parametros.Value = vCODIGO_DEPARTAMENTO;
			parametros = cmd.Parameters.Add("@CODIGO_PAIS", SqlDbType.Int);
			parametros.Value = vCODIGO_PAIS;
			parametros = cmd.Parameters.Add("@NOMBRE_DEPARTAMENTO", SqlDbType.VarChar);
			parametros.Value = vNOMBRE_DEPARTAMENTO;
			cmd.CommandType = CommandType.StoredProcedure;
			if (cmd.ExecuteNonQuery() > 0)
				res = true;
			con.Close();
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return res;
	}


	public Boolean Eliminar(int vCODIGO_DEPARTAMENTO)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spAdm_departamento_E", con);
			parametros = cmd.Parameters.Add("@CODIGO_DEPARTAMENTO", SqlDbType.Int);
			parametros.Value = vCODIGO_DEPARTAMENTO;
			cmd.CommandType = CommandType.StoredProcedure;
			if (cmd.ExecuteNonQuery() > 0)
				res = true;
			con.Close();
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return res;
	}


	public DataSet BuscarTodos()
	{
		SqlConnection con = new SqlConnection();
		DataSet ds = new DataSet();
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spAdm_departamento_TT", con);
			cmd.CommandType = CommandType.StoredProcedure;
			SqlDataAdapter ad = new SqlDataAdapter(cmd);
			ad.Fill(ds);
			con.Close();
			return ds;
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return null;
	}


	public Boolean BuscarCODIGO_DEPARTAMENTO(int vCODIGO_DEPARTAMENTO)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spAdm_departamento_TC", con);
			parametros = cmd.Parameters.Add("@CODIGO_DEPARTAMENTO", SqlDbType.Int);
			parametros.Value = vCODIGO_DEPARTAMENTO;
			cmd.CommandType = CommandType.StoredProcedure;
			SqlDataReader dr = cmd.ExecuteReader();
			while (dr.Read())
			{
				CODIGO_DEPARTAMENTO = dr.GetInt32(0);
				CODIGO_PAIS = dr.GetInt32(1);
				NOMBRE_DEPARTAMENTO = dr.GetString(2);
				res = true;
			}
			con.Close();
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return res;
	}


}
