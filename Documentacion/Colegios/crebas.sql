/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     24/10/2018 10:11:26 p. m.                    */
/*==============================================================*/


if exists (select 1
          from sysobjects
          where  id = object_id('dbo.paAdm_Pais')
          and type in ('P','PC'))
   drop procedure dbo.paAdm_Pais
go

if exists (select 1
          from sysobjects
          where  id = object_id('dbo.paAdm_pais_Nuevo')
          and type in ('P','PC'))
   drop procedure dbo.paAdm_pais_Nuevo
go

if exists (select 1
          from sysobjects
          where  id = object_id('dbo.spUsu_usuarios_TC')
          and type in ('P','PC'))
   drop procedure dbo.spUsu_usuarios_TC
go

if exists (select 1
          from sysobjects
          where  id = object_id('dbo.uspAddUser')
          and type in ('P','PC'))
   drop procedure dbo.uspAddUser
go

if exists (select 1
          from sysobjects
          where  id = object_id('dbo.uspLogin')
          and type in ('P','PC'))
   drop procedure dbo.uspLogin
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('dbo.ADM_DEPARTAMENTO') and o.name = 'FK_ADM_DEPA_REFERENCE_ADM_PAIS')
alter table dbo.ADM_DEPARTAMENTO
   drop constraint FK_ADM_DEPA_REFERENCE_ADM_PAIS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('dbo.ADM_MUNICIPIOS') and o.name = 'FK_ADM_MUNI_REFERENCE_ADM_DEPA')
alter table dbo.ADM_MUNICIPIOS
   drop constraint FK_ADM_MUNI_REFERENCE_ADM_DEPA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('dbo.ALU_ALUMNOS') and o.name = 'FK_ALU_ALUM_REFERENCE_ADM_DEPA')
alter table dbo.ALU_ALUMNOS
   drop constraint FK_ALU_ALUM_REFERENCE_ADM_DEPA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('dbo.ALU_ALUMNOS') and o.name = 'FK_ALU_ALUM_REFERENCE_ADM_MUNI')
alter table dbo.ALU_ALUMNOS
   drop constraint FK_ALU_ALUM_REFERENCE_ADM_MUNI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('dbo.ALU_ALUMNOS') and o.name = 'FK_ALU_ALUM_REFERENCE_ADM_PAIS')
alter table dbo.ALU_ALUMNOS
   drop constraint FK_ALU_ALUM_REFERENCE_ADM_PAIS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('dbo.ALU_ALUMNOS') and o.name = 'FK_ALU_ALUM_REFERENCE_USU_TIPO')
alter table dbo.ALU_ALUMNOS
   drop constraint FK_ALU_ALUM_REFERENCE_USU_TIPO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('dbo.PRO_PROFESORES') and o.name = 'FK_PRO_PROF_REFERENCE_ADM_DEPA')
alter table dbo.PRO_PROFESORES
   drop constraint FK_PRO_PROF_REFERENCE_ADM_DEPA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('dbo.PRO_PROFESORES') and o.name = 'FK_PRO_PROF_REFERENCE_ADM_MUNI')
alter table dbo.PRO_PROFESORES
   drop constraint FK_PRO_PROF_REFERENCE_ADM_MUNI
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('dbo.PRO_PROFESORES') and o.name = 'FK_PRO_PROF_REFERENCE_ADM_PAIS')
alter table dbo.PRO_PROFESORES
   drop constraint FK_PRO_PROF_REFERENCE_ADM_PAIS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('dbo.PRO_PROFESORES') and o.name = 'FK_PRO_PROF_REFERENCE_USU_TIPO')
alter table dbo.PRO_PROFESORES
   drop constraint FK_PRO_PROF_REFERENCE_USU_TIPO
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('dbo.USU_USUARIOS') and o.name = 'FK_USU_USUA_REFERENCE_USU_TIPO')
alter table dbo.USU_USUARIOS
   drop constraint FK_USU_USUA_REFERENCE_USU_TIPO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.ADM_DEPARTAMENTO')
            and   type = 'U')
   drop table dbo.ADM_DEPARTAMENTO
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.ADM_MUNICIPIOS')
            and   type = 'U')
   drop table dbo.ADM_MUNICIPIOS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.ADM_PAIS')
            and   type = 'U')
   drop table dbo.ADM_PAIS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.ALU_ALUMNOS')
            and   type = 'U')
   drop table dbo.ALU_ALUMNOS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.PRO_PROFESORES')
            and   type = 'U')
   drop table dbo.PRO_PROFESORES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.USU_TIPOS_IDENTIFICACION')
            and   type = 'U')
   drop table dbo.USU_TIPOS_IDENTIFICACION
go

if exists (select 1
            from  sysobjects
           where  id = object_id('dbo.USU_USUARIOS')
            and   type = 'U')
   drop table dbo.USU_USUARIOS
go

drop schema dbo
go

/*==============================================================*/
/* User: dbo                                                    */
/*==============================================================*/
create schema dbo
go

/*==============================================================*/
/* Table: ADM_DEPARTAMENTO                                      */
/*==============================================================*/
create table dbo.ADM_DEPARTAMENTO (
   CODIGO_DEPARTAMENTO  int                  not null,
   CODIGO_PAIS          int                  not null,
   NOMBRE_DEPARTAMENTO  varchar(80)          collate Modern_Spanish_CI_AS null,
   constraint PK_ADM_DEPARTAMENTO primary key (CODIGO_DEPARTAMENTO)
         on "PRIMARY"
)
on "PRIMARY"
go

/*==============================================================*/
/* Table: ADM_MUNICIPIOS                                        */
/*==============================================================*/
create table dbo.ADM_MUNICIPIOS (
   CODIGO_MUNICIPIO     int                  not null,
   CODIGO_DEPARTAMENTO  int                  null,
   NOMBRE_MUNICIPIO     varchar(80)          collate Modern_Spanish_CI_AS null,
   constraint PK_ADM_MUNICIPIOS primary key (CODIGO_MUNICIPIO)
         on "PRIMARY"
)
on "PRIMARY"
go

/*==============================================================*/
/* Table: ADM_PAIS                                              */
/*==============================================================*/
create table dbo.ADM_PAIS (
   CODIGO_PAIS          int                  not null,
   NOMBRE_PAIS          varchar(80)          collate Modern_Spanish_CI_AS null,
   constraint PK_ADM_PAIS primary key (CODIGO_PAIS)
         on "PRIMARY"
)
on "PRIMARY"
go

/*==============================================================*/
/* Table: ALU_ALUMNOS                                           */
/*==============================================================*/
create table dbo.ALU_ALUMNOS (
   TIPO_IDENTIFICACION  smallint             not null,
   NUMERO_IDENTIFICACION decimal(15)          not null,
   CODIGO_PAIS          int                  null,
   CODIGO_DEPARTAMENTO  int                  null,
   CODIGO_MUNICIPIO     int                  null,
   PRIMER_NOMBRE        varchar(80)          collate Modern_Spanish_CI_AS null,
   SEGUNDO_NOMBRE       varchar(80)          collate Modern_Spanish_CI_AS null,
   PRIMER_APELLIDO      varchar(80)          collate Modern_Spanish_CI_AS null,
   SEGUNDO_APELLIDO     varchar(80)          collate Modern_Spanish_CI_AS null,
   constraint PK_ALU_ALUMNOS primary key (TIPO_IDENTIFICACION, NUMERO_IDENTIFICACION)
         on "PRIMARY"
)
on "PRIMARY"
go

/*==============================================================*/
/* Table: PRO_PROFESORES                                        */
/*==============================================================*/
create table dbo.PRO_PROFESORES (
   TIPO_IDENTIFICACION  smallint             null,
   CODIGO_PAIS          int                  null,
   CODIGO_DEPARTAMENTO  int                  null,
   CODIGO_MUNICIPIO     int                  null
)
on "PRIMARY"
go

/*==============================================================*/
/* Table: USU_TIPOS_IDENTIFICACION                              */
/*==============================================================*/
create table dbo.USU_TIPOS_IDENTIFICACION (
   TIPO_IDENTIFICACION  smallint             not null,
   DESCRIPCION_TIPO     varchar(40)          collate Modern_Spanish_CI_AS null,
   constraint PK_USU_TIPOS_IDENTIFICACION primary key (TIPO_IDENTIFICACION)
         on "PRIMARY"
)
on "PRIMARY"
go

/*==============================================================*/
/* Table: USU_USUARIOS                                          */
/*==============================================================*/
create table dbo.USU_USUARIOS (
   ID_USUARIO           varchar(20)          collate Modern_Spanish_CI_AS not null,
   Tipo_identificacion  smallint             null,
   Identificacion       decimal(15)          null,
   Nombre               varchar(100)         collate Modern_Spanish_CI_AS null,
   Nivel_Usuario        int                  null,
   Password             varbinary(200)       null,
   constraint PK_USU_USUARIOS primary key (ID_USUARIO)
         on "PRIMARY"
)
on "PRIMARY"
go

alter table dbo.ADM_DEPARTAMENTO
   add constraint FK_ADM_DEPA_REFERENCE_ADM_PAIS foreign key (CODIGO_PAIS)
      references dbo.ADM_PAIS (CODIGO_PAIS)
go

alter table dbo.ADM_MUNICIPIOS
   add constraint FK_ADM_MUNI_REFERENCE_ADM_DEPA foreign key (CODIGO_DEPARTAMENTO)
      references dbo.ADM_DEPARTAMENTO (CODIGO_DEPARTAMENTO)
go

alter table dbo.ALU_ALUMNOS
   add constraint FK_ALU_ALUM_REFERENCE_ADM_DEPA foreign key (CODIGO_DEPARTAMENTO)
      references dbo.ADM_DEPARTAMENTO (CODIGO_DEPARTAMENTO)
go

alter table dbo.ALU_ALUMNOS
   add constraint FK_ALU_ALUM_REFERENCE_ADM_MUNI foreign key (CODIGO_MUNICIPIO)
      references dbo.ADM_MUNICIPIOS (CODIGO_MUNICIPIO)
go

alter table dbo.ALU_ALUMNOS
   add constraint FK_ALU_ALUM_REFERENCE_ADM_PAIS foreign key (CODIGO_PAIS)
      references dbo.ADM_PAIS (CODIGO_PAIS)
go

alter table dbo.ALU_ALUMNOS
   add constraint FK_ALU_ALUM_REFERENCE_USU_TIPO foreign key (TIPO_IDENTIFICACION)
      references dbo.USU_TIPOS_IDENTIFICACION (TIPO_IDENTIFICACION)
go

alter table dbo.PRO_PROFESORES
   add constraint FK_PRO_PROF_REFERENCE_ADM_DEPA foreign key (CODIGO_DEPARTAMENTO)
      references dbo.ADM_DEPARTAMENTO (CODIGO_DEPARTAMENTO)
go

alter table dbo.PRO_PROFESORES
   add constraint FK_PRO_PROF_REFERENCE_ADM_MUNI foreign key (CODIGO_MUNICIPIO)
      references dbo.ADM_MUNICIPIOS (CODIGO_MUNICIPIO)
go

alter table dbo.PRO_PROFESORES
   add constraint FK_PRO_PROF_REFERENCE_ADM_PAIS foreign key (CODIGO_PAIS)
      references dbo.ADM_PAIS (CODIGO_PAIS)
go

alter table dbo.PRO_PROFESORES
   add constraint FK_PRO_PROF_REFERENCE_USU_TIPO foreign key (TIPO_IDENTIFICACION)
      references dbo.USU_TIPOS_IDENTIFICACION (TIPO_IDENTIFICACION)
go

alter table dbo.USU_USUARIOS
   add constraint FK_USU_USUA_REFERENCE_USU_TIPO foreign key (Tipo_identificacion)
      references dbo.USU_TIPOS_IDENTIFICACION (TIPO_IDENTIFICACION)
go


create procedure dbo.paAdm_Pais as
select CODIGO_PAIS, NOMBRE_PAIS from [dbo].[ADM_PAIS];
go


create procedure dbo.paAdm_pais_Nuevo @CODIGO_PAIS INTEGER,
  @NOMBRE_PAIS NVARCHAR(50) as
BEGIN
	BEGIN TRANSACTION;
	BEGIN TRY
		INSERT INTO [dbo].[ADM_PAIS] (CODIGO_PAIS, NOMBRE_PAIS) VALUES (@CODIGO_PAIS,@NOMBRE_PAIS)
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH 
		DECLARE @SQLErrorMessage NVARCHAR(2048);
        SET @SQLErrorMessage = ERROR_MESSAGE();
        RAISERROR (@SQLErrorMessage, 16, 1);
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;

	END CATCH

 END
go


create procedure dbo.spUsu_usuarios_TC @Id_Usuario NVARCHAR(100) as
SELECT ID_USUARIO, Tipo_identificacion, Identificacion, Nombre,  Nivel_Usuario
FROM [dbo].[USU_USUARIOS]
WHERE ID_USUARIO = @Id_Usuario;
go


create procedure dbo.uspAddUser @pLogin NVARCHAR(50), 
     @pPassword NVARCHAR(50),
     @pFirstName NVARCHAR(40) = NULL, 
     @pLastName NVARCHAR(40) = NULL,
     @responseMessage NVARCHAR(250) OUTPUT as
BEGIN
    SET NOCOUNT ON

    DECLARE @salt UNIQUEIDENTIFIER=NEWID()
    BEGIN TRY

        INSERT INTO dbo.[User] (LoginName, PasswordHash, Salt, FirstName, LastName)
        VALUES(@pLogin, HASHBYTES('SHA2_512', @pPassword+CAST(@salt AS NVARCHAR(36))), @salt, @pFirstName, @pLastName)

       SET @responseMessage='Success'

    END TRY
    BEGIN CATCH
        SET @responseMessage=ERROR_MESSAGE() 
    END CATCH

END
go


create procedure dbo.uspLogin @pLoginName NVARCHAR(100),
     @pPassword NVARCHAR(50) as
BEGIN
/*Se va a usar SHA2_512 para encriptar*/
	declare     @responseMessage NVARCHAR(250);
	declare @valido bit;
	declare @Nivel integer; /*1-Usuario,  3-Administrador*/
	declare @sha2 varbinary(8000);
    SET NOCOUNT ON
	set @valido = 0;
	set @Nivel = 1;

    DECLARE @userID nvarchar(100)

    IF EXISTS (SELECT TOP 1 ID_USUARIO FROM [dbo].[USU_USUARIOS] WHERE ID_USUARIO=@pLoginName)
    BEGIN
		set @sha2 = HASHBYTES('SHA2_256',@pPassword);
        SET @userID=(SELECT ID_USUARIO FROM [dbo].[USU_USUARIOS] 
		WHERE ID_USUARIO= @pLoginName
		AND password=@sha2);
--		SELECT @userID, @pLoginName,@pPassword, @sha2, HASHBYTES('SHA2_256',N'jessica') ;
       IF(@userID IS NULL)
           SET @responseMessage='Password Incorrecto'
       ELSE 
	   BEGIN
           SET @responseMessage='Bienvenido Al Sistema'  
		   SET @Nivel=(SELECT Nivel_Usuario FROM [dbo].[USU_USUARIOS] 
		   WHERE ID_USUARIO=@pLoginName AND password=@sha2)
		   set @valido = 1;
	   END;

    END
    ELSE
       SET @responseMessage='Usuario no existe'

select @valido as valido, @responseMessage as respuesta, @Nivel as nivel;
END
go

