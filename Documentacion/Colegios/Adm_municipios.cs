using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

public class Adm_municipios
{
	cnn dbm = new cnn();

	private int CODIGO_MUNICIPIO;
	private int CODIGO_DEPARTAMENTO;
	private string NOMBRE_MUNICIPIO;

	public int CODIGO_MUNICIPIO
	{
		get { return CODIGO_MUNICIPIO; }
		set { CODIGO_MUNICIPIO = value; }
	}
	public int CODIGO_DEPARTAMENTO
	{
		get { return CODIGO_DEPARTAMENTO; }
		set { CODIGO_DEPARTAMENTO = value; }
	}
	public string NOMBRE_MUNICIPIO
	{
		get { return NOMBRE_MUNICIPIO; }
		set { NOMBRE_MUNICIPIO = value; }
	}

	public Adm_municipios(){ }

	public Boolean Crear(int vCODIGO_MUNICIPIO ,int vCODIGO_DEPARTAMENTO ,string vNOMBRE_MUNICIPIO)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spAdm_municipios_N", con);
			parametros = cmd.Parameters.Add("@CODIGO_MUNICIPIO", SqlDbType.Int);
			parametros.Value = vCODIGO_MUNICIPIO;
			parametros = cmd.Parameters.Add("@CODIGO_DEPARTAMENTO", SqlDbType.Int);
			parametros.Value = vCODIGO_DEPARTAMENTO;
			parametros = cmd.Parameters.Add("@NOMBRE_MUNICIPIO", SqlDbType.VarChar);
			parametros.Value = vNOMBRE_MUNICIPIO;
			cmd.CommandType = CommandType.StoredProcedure;
			if (cmd.ExecuteNonQuery() > 0)
				res = true;
			con.Close();
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return res;
	}


	public Boolean Modificar(int vCODIGO_MUNICIPIO ,int vCODIGO_DEPARTAMENTO ,string vNOMBRE_MUNICIPIO)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spAdm_municipios_M", con);
			parametros = cmd.Parameters.Add("@CODIGO_MUNICIPIO", SqlDbType.Int);
			parametros.Value = vCODIGO_MUNICIPIO;
			parametros = cmd.Parameters.Add("@CODIGO_DEPARTAMENTO", SqlDbType.Int);
			parametros.Value = vCODIGO_DEPARTAMENTO;
			parametros = cmd.Parameters.Add("@NOMBRE_MUNICIPIO", SqlDbType.VarChar);
			parametros.Value = vNOMBRE_MUNICIPIO;
			cmd.CommandType = CommandType.StoredProcedure;
			if (cmd.ExecuteNonQuery() > 0)
				res = true;
			con.Close();
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return res;
	}


	public Boolean Eliminar(int vCODIGO_MUNICIPIO)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spAdm_municipios_E", con);
			parametros = cmd.Parameters.Add("@CODIGO_MUNICIPIO", SqlDbType.Int);
			parametros.Value = vCODIGO_MUNICIPIO;
			cmd.CommandType = CommandType.StoredProcedure;
			if (cmd.ExecuteNonQuery() > 0)
				res = true;
			con.Close();
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return res;
	}


	public DataSet BuscarTodos()
	{
		SqlConnection con = new SqlConnection();
		DataSet ds = new DataSet();
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spAdm_municipios_TT", con);
			cmd.CommandType = CommandType.StoredProcedure;
			SqlDataAdapter ad = new SqlDataAdapter(cmd);
			ad.Fill(ds);
			con.Close();
			return ds;
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return null;
	}


	public Boolean BuscarCODIGO_MUNICIPIO(int vCODIGO_MUNICIPIO)
	{
		SqlConnection con = new SqlConnection();
		SqlParameter parametros;
		Boolean res = false;
		try
		{
			con = dbm.getConexion();
			con.Open();
			SqlCommand cmd = new SqlCommand("spAdm_municipios_TC", con);
			parametros = cmd.Parameters.Add("@CODIGO_MUNICIPIO", SqlDbType.Int);
			parametros.Value = vCODIGO_MUNICIPIO;
			cmd.CommandType = CommandType.StoredProcedure;
			SqlDataReader dr = cmd.ExecuteReader();
			while (dr.Read())
			{
				CODIGO_MUNICIPIO = dr.GetInt32(0);
				CODIGO_DEPARTAMENTO = dr.GetInt32(1);
				NOMBRE_MUNICIPIO = dr.GetString(2);
				res = true;
			}
			con.Close();
		}
		catch (SqlException e) { Console.WriteLine("Error de SQL :" + e.Message); }
		catch (Exception e) { Console.WriteLine("Error :" + e.Message); }
		return res;
	}


}
